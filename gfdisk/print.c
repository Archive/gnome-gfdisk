#include <config.h>
#include <gnome.h>
#include <unistd.h>
#include <time.h>
#include "part.h"

#ifdef HAVE_LIBGNOMEPRINT

#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-printer-dialog.h>

static void 
print_tbox(GnomePrintContext *pc, GnomeFont *font, char *text,
	   int x, int y, int x1, int y1, int fill, int align)
{
  double width,height;

  gnome_print_setfont (pc, font);

  if (fill) {
    gnome_print_setrgbcolor (pc, 0.9, 0.8, 0.6);
    gnome_print_moveto (pc,x,y);
    gnome_print_lineto (pc,x1,y);
    gnome_print_lineto (pc,x1,y1);
    gnome_print_lineto (pc,x,y1);
    gnome_print_fill (pc);
  }
  
  gnome_print_setrgbcolor (pc, 0, 0, 0);
  gnome_print_moveto (pc,x,y);
  gnome_print_lineto (pc,x1,y);
  gnome_print_lineto (pc,x1,y1);
  gnome_print_lineto (pc,x,y1);
  gnome_print_closepath (pc);
  gnome_print_stroke (pc);
  
  width = gnome_font_get_width_string (font,text);

  if (align==1) { /* center */
    gnome_print_moveto (pc,x+(x1-x-width)/2,y+(y1-y-font->size/1.5)/2);
  } else {
    gnome_print_moveto (pc,x+font->size/2,y+(y1-y-font->size/1.5)/2);
  }

  gnome_print_setrgbcolor (pc, 0, 0, 0);
  gnome_print_show (pc,text);


}

static void 
print_row(GnomePrintContext *pc, GnomeFont *font,
	  DevInfo *info, PartitionInfo *pi, int *cy) 
{
  int i,row;
  char buf[1000];
  
  if (!pi) { /* print head */
    print_tbox(pc,font,_("Part. #"),50,*cy,95,*cy+20,TRUE,0);
    print_tbox(pc,font,_("Active"),95,*cy,140,*cy+20,TRUE,0);
    print_tbox(pc,font,_("Size(MB)"),140,*cy,210,*cy+20,TRUE,0);
    print_tbox(pc,font,_("Start"),210,*cy,280,*cy+20,TRUE,0);
    print_tbox(pc,font,_("End"),280,*cy,350,*cy+20,TRUE,0);
    print_tbox(pc,font,_("Offset"),350,*cy,400,*cy+20,TRUE,0);
    print_tbox(pc,font,_("ID"),400,*cy,430,*cy+20,TRUE,0);
    print_tbox(pc,font,_("System"),430,*cy,550,*cy+20,TRUE,0);
    *cy-=20;
    return;
  }

  if (pi->ptype!=FREESPACE) {
    sprintf(buf,"%d",pi->pnum+1);
    print_tbox(pc,font,buf,50,*cy,95,*cy+20,FALSE,0);
    if (pi->boot_ind==0x80) sprintf(buf,_("Yes"));
    else sprintf(buf,"");
    print_tbox(pc,font,buf,95,*cy,140,*cy+20,FALSE,1);
    sprintf(buf,"%d",(pi->last_sector-pi->first_sector)/2048);
    print_tbox(pc,font,buf,140,*cy,210,*cy+20,FALSE,0);
    sprintf(buf,"%d",pi->first_sector);
    print_tbox(pc,font,buf,210,*cy,280,*cy+20,FALSE,0);
    sprintf(buf,"%d",pi->last_sector);
    print_tbox(pc,font,buf,280,*cy,350,*cy+20,FALSE,0);
    print_tbox(pc,font,offset_str(info,pi),350,*cy,400,*cy+20,FALSE,0);
    sprintf(buf,"%d",pi->sys_ind);
    print_tbox(pc,font,buf,400,*cy,430,*cy+20,FALSE,0);
    print_tbox(pc,font,get_sys_name(pi->sys_ind),430,*cy,550,*cy+20,FALSE,0);
    *cy-=20;
  }
}

void
print_info(DevInfo *info)
{
  GnomePrintContext *pc;
  GnomeFont *font;
  double matrix[6] = {0.9, 0.1, -0.1, 0.9, 0, 0};
  double matrix2[6] = {1, 0, 0, 1, 0, 100};
  double matrix3[6] = {100, 0, 0, 100, 50, 300};
  double matrix4[6] = {100, 0, 0, 100, 50, 410};
  char img[256][256];
  char colorimg[256][256][3];
  int i,cy, x, y;
  double width,height;
  GnomePrinter *printer;
  char *t,buf[1000],buf1[1000],buf2[1000];
  char hostname[4096];
  time_t ti;
  PartitionInfo *pi,*lpi;

  if (!(printer = gnome_printer_dialog_new_modal ())) return;

  pc = gnome_print_context_new (printer);

  cy = 730;
  font = gnome_font_new ("NimbusRomNo9L-Regu", 36);
  print_tbox(pc,font,
	     _("Harddisk and Partition Info"),
	     50,cy,550,cy+50,TRUE,1);

  gtk_object_unref (GTK_OBJECT (font));
  font = gnome_font_new ("NimbusRomNo9L-Regu", 16);
  cy-=50;
  print_tbox(pc,font,
	     _("Harddisk Info"),
	     50,cy,550,cy+20,TRUE,1);

  gtk_object_unref (GTK_OBJECT (font));
  font = gnome_font_new ("NimbusRomNo9L-Regu", 12);
  cy-=20;
  print_tbox(pc,font,
	     _("Model"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%s",info->model);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Filesystem Path"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%s",info->path);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Capacity"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d MB",(info->heads*info->sectors*info->cylinders)/2048);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Host Type"),
	     50,cy,250,cy+20,FALSE,0);
  (info->htype == H_SCSI) ? sprintf(buf,"SCSI"):sprintf(buf,"IDE");
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Controller Number"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d",info->host);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Device ID"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d",info->did);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Heads"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d",info->heads);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Sectors"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d",info->sectors);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Cylinders"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d",info->cylinders);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);
  cy-=20;
  print_tbox(pc,font,
	     _("Cylinder Size"),
	     50,cy,250,cy+20,FALSE,0);
  sprintf(buf,"%d KB",(info->heads*info->sectors)/2);
  print_tbox(pc,font,
	     buf,
	     250,cy,550,cy+20,FALSE,0);


  gtk_object_unref (GTK_OBJECT (font));
  font = gnome_font_new ("NimbusRomNo9L-Regu", 16);
  cy-=50;
  print_tbox(pc,font,
	     _("Partition Info"),
	     50,cy,550,cy+20,TRUE,1);

  gtk_object_unref (GTK_OBJECT (font));
  font = gnome_font_new ("NimbusRomNo9L-Regu", 12);

  cy-=20;
  print_row(pc,font,info,NULL,&cy);

  pi = info->plist;
  while (pi) {
    if (pi->ptype==EXTENDED) {
      print_row(pc,font,info,pi,&cy);
      lpi=pi->lplist;
      while (lpi) {
	print_row(pc,font,info,lpi,&cy);
	lpi = lpi->next;
      }
    } else {
      print_row(pc,font,info,pi,&cy);
    }
    pi = pi->next;
  }

  gtk_object_unref (GTK_OBJECT (font));
  font = gnome_font_new ("NimbusRomNo9L-Regu", 12);
  gethostname(hostname,4096);
  getdomainname(buf1,1000);
  time(&ti);
  sprintf(buf,_("Printed from %s@%s; Date: %s"),hostname,buf1,ctime(&ti));
  print_tbox(pc,font,
	     buf,
	     50,50,550,70,TRUE,0);


  gnome_print_showpage (pc);
  gnome_print_context_close (pc);

  gtk_object_unref (GTK_OBJECT (font));
  gtk_object_unref (GTK_OBJECT (pc));
  gtk_object_unref (GTK_OBJECT (printer));
}

#endif



