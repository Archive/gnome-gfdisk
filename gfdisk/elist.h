#ifndef ELIST_H
#define ELIST_H


typedef GSList EList;

void    edb_init(char ** texts); 

gint    elist_count(EList *el);
void    elist_append(EList *el, char *name, ...);
void    elist_clear(EList *el);

void esprintf(char *buf, char *name, ...);
void eprintf(char *name, ...);
void eview(char *name, ...);
void eview_list(EList *el, char *name, ...);

void ehelp_goto(void *ignore, char *topic);

#endif
