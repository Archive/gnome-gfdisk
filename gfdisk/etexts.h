
char *etexts[] = {
  "E-WRITE_NOT_ENABLED%s",N_("Writing to this device is not enabled.\n"
			     "(ALLOW_WRITE = %s)"),
  "I-NOT_IMPLEMENTED",N_("Sorry, not implemented yet."),
  "I-NO_PART_SEL",N_("No partition selected."),
  "E-MAX_PART",N_("Maximum number of partitions reached."),
  "E-NEED_FREE_SPACE",N_("Please select a free space."),
  "I-NEED_REBOOT",N_("Wrote partition table, but re-read table failed.\n"  
		     "Reboot the system to ensure the partition table\n"
		     "is correctly updated."),
  "I-WROTE_TABLE",N_("Wrote partition table to disk."),
  "E-WRITE_FAILED",N_("Writing partition table failed."),
  "E-SECOND_EXT",N_("Extended partition already exists."),
  "I-MODIFIED",N_("Some partitions have been modified. \n"
		  "Do you realy want to exit Gnome FDisk?"),
  "E-NODEV",N_("No devices found - make sure that you have "
	       "permissions to access the devices.\n On many "
	       "systems only root is allowed to partition "
	       "the disks."),

  "E-TEST","Test Error",
  "W-TEST","Test Warning",
  "I-TEST","Test Info",
  /*"",N_(""),*/
  NULL,NULL
};
