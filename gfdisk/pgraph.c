#include <math.h>
#include <stdio.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>

#include "pgraph.h"
#include "part.h"

#define PSPACEX 20
#define PSPACEY 20
#define PMINF 10

static void pgraph_init          (PGraph         *pgraph);
static void pgraph_class_init    (PGraphClass    *class);
static void pgraph_destroy       (GtkObject      *object);
static void pgraph_realize       (GtkWidget      *widget);
static void pgraph_size_request  (GtkWidget      *widget,
				  GtkRequisition *requisition);
static gint pgraph_expose        (GtkWidget      *widget,
				  GdkEventExpose *event);
static void pgraph_draw          (GtkWidget      *widget,
				  GdkRectangle   *area);
static void pgraph_size_allocate (GtkWidget      *widget,
				  GtkAllocation  *allocation);
static gint pgraph_button_press  (GtkWidget      *widget,
				  GdkEventButton *event);


static guint pgraph_signals[1] = {0};

static GtkWidgetClass *parent_class = NULL;

typedef void (*PGraphSignal1) (GtkObject * object,
			       gpointer *arg1,
			       gpointer data);

static void
pgraph_marshal_signal_1 (GtkObject * object,
			 GtkSignalFunc func,
			 gpointer func_data,
			 GtkArg * args)
{
  PGraphSignal1 rfunc;

  rfunc = (PGraphSignal1) func;

  (*rfunc) (object, GTK_VALUE_POINTER (args[0]),
	    func_data);
}

guint
pgraph_get_type ()
{
  static guint pgraph_type = 0;

  if (!pgraph_type)
    {
      GtkTypeInfo pgraph_info =
      {
	"PGraph",
	sizeof (PGraph),
	sizeof (PGraphClass),
	(GtkClassInitFunc) pgraph_class_init,
	(GtkObjectInitFunc) pgraph_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      pgraph_type = gtk_type_unique (gtk_widget_get_type (), &pgraph_info);
    }

  return pgraph_type;
}

static void
pgraph_class_init (PGraphClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (gtk_widget_get_type ());

  pgraph_signals[0]=
    gtk_signal_new ("select_partition",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (PGraphClass, select_partition),
		    pgraph_marshal_signal_1,
		    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, pgraph_signals,1);

  object_class->destroy = pgraph_destroy;
  widget_class->realize = pgraph_realize;
  widget_class->draw = pgraph_draw;
  widget_class->expose_event = pgraph_expose;
  widget_class->size_request = pgraph_size_request;
  widget_class->size_allocate = pgraph_size_allocate;
  widget_class->button_press_event = pgraph_button_press;

  class->select_partition = NULL;

}

static void
pgraph_init (PGraph *pgraph)
{
  pgraph->info = NULL;
  pgraph->sel = NULL;
  pgraph->offset = 0;
  pgraph->pheight = 100;
  pgraph->pminx = 55;
}

GtkWidget*
pgraph_new ()
{
  PGraph *pgraph;

  pgraph = gtk_type_new (pgraph_get_type ());

  return GTK_WIDGET (pgraph);
}

static void
pgraph_destroy (GtkObject *object)
{
  PGraph *pgraph;

  g_return_if_fail (object != NULL);
  g_return_if_fail (IS_PGRAPH (object));

  pgraph = PGRAPH (object);
 
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);

  if (pgraph->gc) gdk_gc_destroy(pgraph->gc);
}

static void
pgraph_realize (GtkWidget *widget)
{
  PGraph *pgraph;
  GdkWindowAttr attributes;
  gint attributes_mask;
  gint fh;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PGRAPH (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  pgraph = PGRAPH (widget);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events (widget) | 
    GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
    GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
    GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new (widget->parent->window, 
				   &attributes, attributes_mask);

  widget->style = gtk_style_attach (widget->style, widget->window);

  gdk_window_set_user_data (widget->window, widget);

  pgraph->gc=gdk_gc_new(widget->window);
  
  pgraph->co[0].red=65535;
  pgraph->co[0].green=0;
  pgraph->co[0].blue=0;
  gdk_color_alloc(attributes.colormap,&pgraph->co[0]);

  pgraph->co[1].red=0x1861;
  pgraph->co[1].green=0x9248;
  pgraph->co[1].blue=0xffff;
  gdk_color_alloc(attributes.colormap,&pgraph->co[1]);

  pgraph->co[2].red=0;
  pgraph->co[2].green=0;
  pgraph->co[2].blue=0xffff;
  gdk_color_alloc(attributes.colormap,&pgraph->co[2]);

  fh = widget->style->font->ascent+widget->style->font->descent;
  pgraph->pheight = 4*fh+8+2*PSPACEY;
  pgraph->pminx = gdk_string_width (widget->style->font, "0000MB")+8;
}


static int
pgraph_min_width(PGraph *pgraph)
{
  PartitionInfo *pi,*ext;
  int lx;

  if (pgraph->info) {    
    pi = pgraph->info->plist;
    lx=2*PSPACEX;
    while(pi) {
      if (pi->ptype==EXTENDED) {
	ext=pi->lplist;
	while(ext) {
	  if (ext->ptype!=FREESPACE) lx=lx+pgraph->pminx;
	  else lx=lx+PMINF;
	  ext=ext->next;
	}
      } else {
	if (pi->ptype!=FREESPACE) lx=lx+pgraph->pminx;
	else lx=lx+PMINF;
      }
      pi=pi->next;
    }
    return(lx);
  } else {
    return(2*PSPACEX+pgraph->pminx);
  }
}

static void 
pgraph_size_request (GtkWidget      *widget,
		     GtkRequisition *requisition)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PGRAPH (widget));

  requisition->width = pgraph_min_width(PGRAPH (widget));
  requisition->height = PGRAPH(widget)->pheight;

}

static void
pgraph_size_allocate (GtkWidget     *widget,
		      GtkAllocation *allocation)
{
  PGraph *pgraph;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PGRAPH (widget));
  g_return_if_fail (allocation != NULL);

  widget->allocation = *allocation;
  if (GTK_WIDGET_REALIZED (widget))
    {
      pgraph = PGRAPH (widget);

      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);
    }
}

static gint
getppos(GtkWidget *widget, PartitionInfo *pi, int *start, int *end,float cor)
{
  PGraph *pgraph;
  PartitionInfo *tmp,*ext;
  int psize,pnum;
  gint x,x1,mx,lx,elx,enx,nx,found,minx,dx;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_PGRAPH (widget), FALSE);

  found=FALSE;
  pgraph = PGRAPH (widget);


  psize=pgraph->info->heads*pgraph->info->sectors*pgraph->info->cylinders;
  mx=widget->allocation.width-2*PSPACEX;

  tmp = pgraph->info->plist;
  lx=PSPACEX;
  nx=PSPACEX;

  pnum=0;
  while(tmp) {
    if (tmp->ptype==EXTENDED) {
      ext=tmp->lplist;
      elx=lx;
      while(ext) {
	x=PSPACEX+((float)ext->first_sector/psize)*mx;
	x1=PSPACEX+((float)ext->last_sector/psize)*mx;
	if (ext->ptype!=FREESPACE) {
	  if ((x1-x)*cor<pgraph->pminx) enx=elx+pgraph->pminx;
	  else enx=elx+(x1-x)*cor; 
	} else {
	  if ((x1-x)*cor<PMINF) enx=elx+PMINF;
	  else enx=elx+(x1-x)*cor; 
	}
	if (ext==pi) {
	  found=TRUE;
	  *start=elx;
	  *end=enx;
	}
	elx=enx;
	ext=ext->next;
	pnum++;
      }
      nx=enx;
    } else {
      pnum++;
      x=PSPACEX+((float)tmp->first_sector/psize)*mx;
      x1=PSPACEX+((float)tmp->last_sector/psize)*mx;
    
      if (tmp->ptype!=FREESPACE) {
	if ((x1-x)*cor<pgraph->pminx) nx=lx+pgraph->pminx;
	else nx=lx+(x1-x)*cor; 
      } else {
	if ((x1-x)*cor<PMINF) nx=lx+PMINF;
	else nx=lx+(x1-x)*cor; 
      }
    }
    if (tmp==pi) {
      found=TRUE;
      *start=lx;
      *end=nx;
    }
    lx=nx;
    tmp=tmp->next;
  }
  lx=lx+PSPACEX;

  if (found) {
    minx=pgraph_min_width(pgraph);
    if (lx>widget->allocation.width) {
      if (minx<lx) {
	cor=(cor*widget->allocation.width)/lx;
	return(getppos(widget,pi,start,end,cor));
      }
    }
  }

  *start-=pgraph->offset;
  *end-=pgraph->offset;

  return(found);
}

static void highlight_selected(GtkWidget *widget)
{
  PGraph *pgraph;
  int start,end;
  gint y,h;

  pgraph = PGRAPH (widget);

  if (widget->allocation.height<pgraph->pheight) {
    y = (widget->allocation.height-(pgraph->pheight-2*PSPACEY))/2;
  } else {
    y = PSPACEY;
  }

  h = widget->allocation.height;
  if (y<2) return;

  if (getppos(widget,pgraph->sel,&start,&end,1.0)) {
    gdk_gc_set_foreground(pgraph->gc,&pgraph->co[0]);
    gdk_draw_rectangle (widget->window,
			pgraph->gc,0,
			start,y,
			end-start,
			h-2*y);

  }
}

static gint
draw_pi(GtkWidget *widget,PartitionInfo *pi)
{
  PGraph *pgraph;
  GdkRectangle clip;
  int start,end,tl,fh;
  char buf[100];
  gint y,h;

  pgraph = PGRAPH (widget);

  if (widget->allocation.height<pgraph->pheight) {
    y = (widget->allocation.height-(pgraph->pheight-2*PSPACEY))/2;
  } else {
    y = PSPACEY;
  }

  h = widget->allocation.height;
  if (y<2) return;

  getppos(widget,pi,&start,&end,1.0);

  fh=widget->style->font->ascent+widget->style->font->descent;


  gdk_draw_rectangle (widget->window,
		      widget->style->black_gc,0,
			start,y,
			end-start,
			h-2*y);

  if (is_ext_pi(pgraph->info,pi)) {
    if (pi->ptype!=FREESPACE) {
      gdk_draw_rectangle (widget->window,
			  widget->style->white_gc,1,
			  start+1,y+fh+1,
			  end-start-1,
			  h-2*y-fh-1);

      gdk_gc_set_foreground(pgraph->gc,&pgraph->co[2]);
      gdk_draw_rectangle (widget->window,
			  pgraph->gc,1,
			  start+1,y+1,
			  end-start-1,
			  fh/2);
      gdk_gc_set_foreground(pgraph->gc,&pgraph->co[1]);
      gdk_draw_rectangle (widget->window,
			  pgraph->gc,1,
			  start+1,y+fh/2+1,
			  end-start-1,
			  fh/2);
    } else {
      gdk_draw_rectangle (widget->window,
			  widget->style->white_gc,1,
			  start+1,y+fh/2+1,
			  end-start-1,
			  h-2*y-fh/2-1);
      gdk_gc_set_foreground(pgraph->gc,&pgraph->co[2]);
      gdk_draw_rectangle (widget->window,
			  pgraph->gc,1,
			  start+1,y+1,
			  end-start-1,
			  fh/2);

    }
  } else {
    if (pi->ptype!=FREESPACE) {
      gdk_draw_rectangle (widget->window,
			  widget->style->white_gc,1,
			  start+1,y+fh+1,
			  end-start-1,
			  h-2*y-fh-1);
      gdk_gc_set_foreground(pgraph->gc,&pgraph->co[2]);
      gdk_draw_rectangle (widget->window,
			  pgraph->gc,1,
			  start+1,y+1,
			  end-start-1,
			  fh);
    } else {
      gdk_draw_rectangle (widget->window,
			  widget->style->white_gc,1,
			  start+1,y+1,
			  end-start-1,
			  h-2*y-1);
    }
  }

  clip.x = start+1; clip.y = y+1;
  clip.width = end-start-2; clip.height = h-2*y-2;
  gdk_gc_set_clip_rectangle (widget->style->black_gc,&clip);

  if (pi->ptype!=FREESPACE) {

    sprintf(buf,"%s%d",&pgraph->info->path[5],pi->pnum+1);
    tl=strlen(buf);
    gdk_draw_text(widget->window,widget->style->font,
		  widget->style->black_gc,
		  start+4,h-y-4-fh,buf,tl);

    sprintf(buf,"%dMB",(pi->last_sector-pi->first_sector)/2048);
    tl=strlen(buf);
    gdk_draw_text(widget->window,widget->style->font,
		  widget->style->black_gc,
		  start+4,h-y-4,buf,tl);

    if (pi->label) {
      tl=strlen(pi->label);
      gdk_draw_text(widget->window,widget->style->font,
		    widget->style->black_gc,
		    start+4,h-y-4-2*fh,pi->label,tl);
    }
  }

  clip.x = 0; clip.y = 0;
  clip.width = -1; 
  clip.height = -1;
  gdk_gc_set_clip_rectangle (widget->style->black_gc,&clip);

  return(end);
}

static gint
pgraph_expose (GtkWidget *widget, GdkEventExpose *event)
{
  PGraph *pgraph;
  PartitionInfo *pi,*ext;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_PGRAPH (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (event->count > 0)
    return FALSE;

  if (GTK_WIDGET_DRAWABLE (widget)) pgraph_draw(widget,&event->area);
   
  return FALSE;
}

void           
pgraph_set_info(PGraph *pgraph, DevInfo *di)
{
  g_return_if_fail (pgraph != NULL);
  g_return_if_fail (IS_PGRAPH (pgraph));

  pgraph->info=di;
  gtk_widget_queue_resize (GTK_WIDGET(pgraph));
  gtk_widget_draw (GTK_WIDGET(pgraph), NULL);
}

void           
pgraph_set_offset(PGraph *pgraph,
		  gint offset)
{
  g_return_if_fail (pgraph != NULL);
  g_return_if_fail (IS_PGRAPH (pgraph));

  pgraph->offset = offset;
  gtk_widget_draw (GTK_WIDGET(pgraph), NULL);
}

static gint
pgraph_button_press (GtkWidget      *widget,
			 GdkEventButton *event)
{
  PGraph *pgraph;
  gint x,y;
  int start,end;
  PartitionInfo *pi,*ext;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_PGRAPH (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  pgraph = PGRAPH (widget);

  x=event->x;
  y=event->y;

  if (pgraph->info && (event->button==1)) {

    pi = pgraph->info->plist;
    while(pi) {
      if (pi->ptype==EXTENDED) {
	ext=pi->lplist;
	while(ext) {
	  getppos(widget,ext,&start,&end,1.0);
	  if ((x>=start)&&(x<=end)&&
	      (y>=PSPACEY)&&(y<=pgraph->pheight-PSPACEY)&&
	      (ext!=pgraph->sel)) {
	    pgraph->sel=ext;
	    gtk_widget_draw (GTK_WIDGET(pgraph), NULL);
	    gtk_signal_emit (GTK_OBJECT (pgraph), pgraph_signals[0],ext);
	    break;
	  }
	  ext=ext->next;
	}
      } else {
	getppos(widget,pi,&start,&end,1.0);
	if ((x>=start)&&(x<=end)&&
	    (y>=PSPACEY)&&(y<=pgraph->pheight-PSPACEY)&&
	    (pi!=pgraph->sel)) {
	  pgraph->sel=pi;
	  gtk_widget_draw (GTK_WIDGET(pgraph), NULL);
	  gtk_signal_emit (GTK_OBJECT (pgraph), pgraph_signals[0],pi);
	  break;
	}
      }
      pi=pi->next;
    }

  }
  return FALSE;
}

gint
pgraph_select_pi(PGraph *pgraph, PartitionInfo *pi)
{
  PartitionInfo *tmp,*ext;

  g_return_val_if_fail (pgraph != NULL, FALSE);
  g_return_val_if_fail (IS_PGRAPH(pgraph), FALSE);

  if (pgraph->info && (pgraph->sel!=pi)) {
    tmp = pgraph->info->plist;
  
    while(tmp) {
      if (tmp->ptype==EXTENDED) {
	ext=tmp->lplist;
	while(ext){
	  if (ext==pi) {
	    pgraph->sel=ext;
	    gtk_widget_draw (GTK_WIDGET(pgraph), NULL);
	    break;
	  }
	  ext=ext->next;
	}
      }
      if (tmp==pi) {
	pgraph->sel=tmp;
	gtk_widget_draw (GTK_WIDGET(pgraph), NULL);
	break;
      }
      tmp=tmp->next;
    }
  }

  return TRUE;
}

static void
pgraph_draw (GtkWidget *widget, GdkRectangle *area)
{
  PGraph *pgraph;
  PartitionInfo *pi,*ext;
  gint end,x,y,h;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PGRAPH(widget));
  g_return_if_fail (area != NULL);

  end = 0;
  pgraph = PGRAPH (widget);

  gdk_window_set_back_pixmap(widget->window, NULL, TRUE);
  if (widget->allocation.height<pgraph->pheight) {
    y = (widget->allocation.height-(pgraph->pheight-2*PSPACEY))/2;
    h = pgraph->pheight-2*PSPACEY;
  } else {
    y = PSPACEY;
    h = widget->allocation.height-2*PSPACEY;
  }
  gdk_window_clear_area(widget->window,2,2,
			widget->allocation.width-4,
			y-2);
  gdk_window_clear_area(widget->window,2,y+h+1,
			widget->allocation.width-4,
			widget->allocation.height-y-h-3);

  if (pgraph->offset<PSPACEX)
    gdk_window_clear_area(widget->window,2,2,
			  PSPACEX-pgraph->offset-2,
			  widget->allocation.height-4);
		  
  if (pgraph->info) {

    pi = pgraph->info->plist;
    while(pi) {
      if (pi->ptype==EXTENDED) {
	ext=pi->lplist;
	while (ext) {
	  end = draw_pi(widget,ext);
	  ext=ext->next;
	}
      } else {
	end = draw_pi(widget,pi);
      }
      pi=pi->next;
    }

    highlight_selected(widget);
  }

  if (end<(widget->allocation.width-2))
    gdk_window_clear_area(widget->window,end+1,2,
			  widget->allocation.width-end-3,
			  widget->allocation.height-4);

  gtk_draw_shadow (widget->style, widget->window,
		   GTK_WIDGET_STATE (widget), GTK_SHADOW_IN,
		   0, 0,
		   widget->allocation.width,
		   widget->allocation.height);
		   
}
