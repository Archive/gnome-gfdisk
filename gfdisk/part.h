#ifndef PART_H
#define PART_H

#include <sys/types.h>
#include <linux/genhd.h>

#define SECTOR_SIZE 512

#define PT_FREE_SPACE 0x00

#define PT_LINUX_EXTENDED 0x85
#define PT_LINUX_MINIX 0x81
#define PT_LINUX_SWAP 0x82
#define PT_LINUX 0x83

#define PT_DOS_EXTENDED 0x05
#define PT_DOS_FAT12 0x01
#define PT_DOS_FAT16 0x04
#define PT_DOS_FAT16_BIG 0x06
#define PT_DOS_HIDDEN_FAT12 0x11
#define PT_DOS_HIDDEN_FAT16 0x14
#define PT_DOS_HIDDEN_FAT16_BIG 0x16
#define PT_DOS_ACCESS 0xe1
#define PT_DOS_RO 0xe3
#define PT_DOS_SECONDARY 0xf2

#define PT_WIN95_FAT32 0x0b
#define PT_WIN95_FAT32_LBA 0x0c
#define PT_WIN95_FAT16_LBA 0x0e
#define PT_WIN95_EXT_LBA 0x0f

#define PT_OS2_HPFS 0x07
#define PT_OS2_BOOT 0x0a

#define PT_GNU_HURD 0x63
#define PT_MINIX_OLD 0x80
#define PT_BSD_386 0xa5
#define PT_NOVEL_NETWARE_286 0x64
#define PT_NOVEL_NETWARE_386 0x65
#define PT_NOVEL 0x51

#define PT_XENIX_ROOT 0x02
#define PT_XENIX_USR 0x03
#define PT_AIX 0x08
#define PT_AIX_BOOT 0x09
#define PT_VENIX 0x40

#define PT_MICROPORT 0x52
#define PT_PC_IX 0x75
#define PT_AMOEBA 0x93
#define PT_AMOEBA_BBT 0x94
#define PT_BSDI_FS 0xb7
#define PT_BSDI_SWAP 0xb8
#define PT_SYRINX 0xC7
#define PT_CPM 0xdb
#define PT_BBT 0xff

#define is_dos_partition(x) ((x) == 1 || (x) == 4 || (x) == 6)

typedef enum 
{
  PRIMARY,
  EXTENDED,
  LOGICAL,
  FREESPACE
} PType;

typedef enum 
{
  PRI,
  LOG,
  PRILOG,
  UNUSABLE,
  USED
} PUsage;

typedef struct PartitionInfo {
  PType ptype;
  unsigned char sys_ind;
  unsigned char boot_ind;
  int pnum;
  int first_sector;
  int last_sector;
  int offset;
  char *label;
  struct PartitionInfo *prev,*next,*lplist;
} PartitionInfo;

typedef enum
{
  H_SCSI,
  H_IDE
} HType;

typedef struct {
  char *model;
  char *path;
  short heads,sectors,cylinders;
  short host,did;
  HType htype;
  PartitionInfo *plist;
  char changed;
} DevInfo;


#define is_extended(x)	((x)==PT_DOS_EXTENDED||(x)==PT_LINUX_EXTENDED)

typedef union {
    struct {
	unsigned char align[2];
	unsigned char b[SECTOR_SIZE];
    } c;
    struct {
	unsigned char align[2];
	unsigned char buffer[0x1BE];
	struct partition part[4];
	unsigned short flag;
    } p;
} PartitionTable;

char *get_sys_name(unsigned char sys_ind);
PartitionInfo *new_pinfo(PType ptype, int fs, int ls); 
PartitionInfo *ext_part(DevInfo *info);
int is_ext_pi(DevInfo *info,PartitionInfo *pi);
int pnum_in_use(DevInfo *info,int pnum);
int primary_count(DevInfo *info);
void maximize_partition(DevInfo *info, PartitionInfo *pi);
int enumerate_logical(DevInfo *info);
int inactivate_all_partitions(DevInfo *info);
int delete_all_logical(DevInfo *info);
int delete_part(DevInfo *info,PartitionInfo *pi);
int delete_all_partitions(DevInfo *info);
PartitionInfo * add_part(DevInfo *info, PType ptype, unsigned char sys_ind,
			 unsigned char boot_ind, 
			 int pnum, int fs, int ls, int offset);
int read_pt(DevInfo *info);
void minimize_ext(DevInfo *info);
char *offset_str(DevInfo *info,PartitionInfo *pi);

#endif
