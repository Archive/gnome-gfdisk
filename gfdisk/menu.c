#include <config.h>
#include <gnome.h>

#include "part.h"
#include "gfdisk.h"
#include "pview.h"

void set_ptype_names(GnomeUIInfo ui[])
{
  int t,i = 0;

  while(ui[i].type!=GNOME_APP_UI_ENDOFINFO) {
    if (ui[i].type==GNOME_APP_UI_SUBTREE) {
      set_ptype_names((GnomeUIInfo *)ui[i].moreinfo);
    } else {
      if ((ui[i].type==GNOME_APP_UI_ITEM) && (ui[i].label==NULL)) {
	t = (int)ui[i].user_data;
	if ((t>=0)&&(t<256)) {
	  ui[i].label=get_sys_name(t);
	  ui[i].moreinfo=(gpointer)set_sysind_cb;
	}
      }
    }
    i++;
  }
}

void setup_accelerators(GnomeUIInfo ui[])
{
  int i = 0;
  guchar key;
  guint8 mod;

  while(ui[i].type!=GNOME_APP_UI_ENDOFINFO) {
    if (ui[i].type==GNOME_APP_UI_SUBTREE) {
      setup_accelerators((GnomeUIInfo *)ui[i].moreinfo);
    } else {
      if ((ui[i].type==GNOME_APP_UI_ITEM) && 
	  (ui[i].pixmap_type==GNOME_APP_PIXMAP_STOCK)) {
	if (gnome_stock_menu_accel(ui[i].pixmap_info, &key, &mod)) {
	  ui[i].accelerator_key = key;
	  ui[i].ac_mods = mod;
	}
      }
    }
    i++;
  }
}

#define PT_ITEM(ptype) \
   {GNOME_APP_UI_ITEM,NULL,NULL,NULL, \
   (gpointer)ptype,NULL,GNOME_APP_PIXMAP_NONE,NULL, \
   0, (GdkModifierType)0, NULL}

static GnomeUIInfo filemenu[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("_Save Table"),
			 N_("Save the partition table to the harddisk"),
			 partition_save_cb,GNOME_STOCK_MENU_SAVE),
  GNOMEUIINFO_ITEM_STOCK(N_("_Reload Table"),
			 N_("Reload the partition table from the harddisk"),
			 reload_cb,GNOME_STOCK_MENU_OPEN),
  GNOMEUIINFO_ITEM_NONE(N_("_Clear Table"),
			N_("Delete all partitions"),
			delete_all_partitions_cb),
#ifdef HAVE_LIBGNOMEPRINT
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("_Print..."),
			 N_("Print partition and hardisk information"),
			 print_cb,GNOME_STOCK_MENU_PRINT),
#endif
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("_Exit"),
			 N_("Exit Gnome FDisk"),
			 exit_cb,GNOME_STOCK_MENU_EXIT),
  GNOMEUIINFO_END
};

static GnomeUIInfo ptmiscmenu[] = {
  PT_ITEM(PT_OS2_HPFS),
  PT_ITEM(PT_OS2_BOOT),
  PT_ITEM(PT_MICROPORT),
  PT_ITEM(PT_PC_IX),
  PT_ITEM(PT_AMOEBA),
  PT_ITEM(PT_AMOEBA_BBT),
  PT_ITEM(PT_BBT),
  PT_ITEM(PT_CPM),
  GNOMEUIINFO_END
};

static GnomeUIInfo ptunixmenu[] = {
  PT_ITEM(PT_LINUX_EXTENDED),
  PT_ITEM(PT_LINUX_MINIX),
  PT_ITEM(PT_MINIX_OLD),
  PT_ITEM(PT_GNU_HURD),
  GNOMEUIINFO_SEPARATOR,
  PT_ITEM(PT_BSD_386),
  PT_ITEM(PT_NOVEL_NETWARE_386),
  PT_ITEM(PT_NOVEL_NETWARE_286),
  PT_ITEM(PT_NOVEL),
  PT_ITEM(PT_XENIX_ROOT),
  PT_ITEM(PT_XENIX_USR),
  PT_ITEM(PT_AIX),
  PT_ITEM(PT_AIX_BOOT),
  PT_ITEM(PT_VENIX),
  PT_ITEM(PT_SYRINX),
  PT_ITEM(PT_BSDI_FS),
  PT_ITEM(PT_BSDI_SWAP),
  GNOMEUIINFO_END
};

static GnomeUIInfo ptdosmenu[] = {
  PT_ITEM(PT_DOS_FAT16_BIG),
  PT_ITEM(PT_DOS_FAT16),
  PT_ITEM(PT_DOS_FAT12),
  PT_ITEM(PT_DOS_HIDDEN_FAT16_BIG),
  PT_ITEM(PT_DOS_HIDDEN_FAT16),
  PT_ITEM(PT_DOS_HIDDEN_FAT12),
  PT_ITEM(PT_DOS_ACCESS),
  PT_ITEM(PT_DOS_RO),
  PT_ITEM(PT_DOS_SECONDARY),
  GNOMEUIINFO_SEPARATOR,
  PT_ITEM(PT_WIN95_FAT32),
  PT_ITEM(PT_WIN95_FAT32_LBA),
  PT_ITEM(PT_WIN95_FAT16_LBA),
  PT_ITEM(PT_WIN95_EXT_LBA),
  GNOMEUIINFO_END
};

static GnomeUIInfo ptmenu[] = {
  PT_ITEM(PT_LINUX),
  PT_ITEM(PT_LINUX_SWAP),
  GNOMEUIINFO_SEPARATOR,
  PT_ITEM(PT_DOS_EXTENDED),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_SUBTREE(N_("DOS/Windows"),ptdosmenu),
  GNOMEUIINFO_SUBTREE(N_("Unix"),ptunixmenu),
  GNOMEUIINFO_SUBTREE(N_("Miscellaneous"),ptmiscmenu),
  GNOMEUIINFO_END
};

static GnomeUIInfo partitionmenu[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("_Create..."),
			 N_("Create a new partition"),
			 partition_create_cb,GNOME_STOCK_MENU_PASTE),
  GNOMEUIINFO_ITEM_STOCK(N_("_Delete"),
			 N_("Delete the selected partition"),
			 partition_delete_cb,GNOME_STOCK_MENU_CUT),
  GNOMEUIINFO_SUBTREE_HINT(N_("_Type"),
			   N_("Set the partition type"),
			   ptmenu),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("_Maximize"),
			 N_("Maximize the selected partition"),
			 maximize_cb,GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_ITEM_STOCK(N_("_Activate"),
			 N_("Activate the selected partition"),
			 activate_cb,GNOME_STOCK_MENU_BLANK),
  GNOMEUIINFO_END
};

static GnomeUIInfo unitmenusub[] = {
  GNOMEUIINFO_ITEM_DATA(N_("_Cylinders"),
			NULL,
			set_units_cb,
			(gpointer)UNIT_CYLINDERS,NULL),
  GNOMEUIINFO_ITEM_DATA(N_("_Sectors"),NULL,set_units_cb,
			     (gpointer)UNIT_SECTORS,NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo unitmenu[] = {
  GNOMEUIINFO_RADIOLIST(unitmenusub),
  GNOMEUIINFO_END
};

static GnomeUIInfo helpmenu[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("_About"),
			 N_("Copyright Information"),
			 about_cb,GNOME_STOCK_MENU_ABOUT),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_HELP("gfdisk"),
  GNOMEUIINFO_END
};


static GnomeUIInfo optionsmenu[] = {
  GNOMEUIINFO_SUBTREE_HINT(N_("_Units"),
			   N_("Select the display unit"),
			   unitmenu),
  GNOMEUIINFO_SEPARATOR,
#define MODE_INDEX 2 
  GNOMEUIINFO_TOGGLEITEM(N_("_Advanced mode"),
			 N_("Toggle between advanced and normal mode"),
			 toggle_mode_cb,NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("_Device Information..."),
			N_("Displays information about the device"),
			devinfo_cb,GNOME_STOCK_MENU_BOOK_OPEN),
  GNOMEUIINFO_END
};

void set_advanced_mode() 
{
  gtk_check_menu_item_set_state(
       GTK_CHECK_MENU_ITEM(optionsmenu[MODE_INDEX].widget),
       GTK_STATE_ACTIVE);
}

static GnomeUIInfo fdiskmenu[] = {
  GNOMEUIINFO_SUBTREE(N_("_File"),filemenu),
  GNOMEUIINFO_SUBTREE(N_("_Partition"),partitionmenu),
  GNOMEUIINFO_SUBTREE(N_("_Options"),optionsmenu),
  GNOMEUIINFO_SUBTREE(N_("_Help"),helpmenu),
  GNOMEUIINFO_END
};

create_menus(GnomeApp *app) {
  set_ptype_names(ptmenu);
  setup_accelerators(fdiskmenu);
  gnome_app_create_menus(app,fdiskmenu);
  gnome_app_install_menu_hints(app,fdiskmenu);
}




