#include <string.h>

#include <config.h>
#include <gnome.h>

#include "gtk/gtksignal.h"
#include "pgraph.h"
#include "spgraph.h"
#include "pview.h"
#include "part.h"

static void pview_class_init          (PViewClass *klass);
static void pview_init                (PView      *pview);

enum 
{
  CLC_NUM,
  CLC_ACTIVE,
  CLC_SIZE,
  CLC_START,
  CLC_END,
  CLC_OFFSET,
  CLC_ID,
  CLC_SYSTEM,
  CLC_LAST
};

static char *cl_names[CLC_LAST] = {
  [CLC_NUM] = N_("Part. #"),
  [CLC_SIZE] =  N_("Size(MB)"),
  [CLC_ACTIVE] = N_("Active"),
  [CLC_START] = N_("Start"),
  [CLC_END] = N_("End"),
  [CLC_OFFSET] = N_("Offset"),
  [CLC_ID] = N_("ID"),
  [CLC_SYSTEM] = N_("System")
};

guint
pview_get_type ()
{
  static guint pview_type = 0;

  if (!pview_type)
    {
      GtkTypeInfo pview_info =
      {
	"PView",
	sizeof (PView),
	sizeof (PViewClass),
	(GtkClassInitFunc) pview_class_init,
	(GtkObjectInitFunc) pview_init,
        (GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL
      };

      pview_type = gtk_type_unique (gtk_vbox_get_type (), &pview_info);
    }

  return pview_type;
}

static void
pview_class_init (PViewClass *class)
{
  GtkObjectClass *object_class;

  object_class = (GtkObjectClass*) class;
  
}

static void 
clist_select_row(PView *pview, int sel)
{
  PartitionInfo *pi;

  pview->sel=NULL;

  if (sel>=GTK_CLIST(pview->clist)->rows) sel=GTK_CLIST(pview->clist)->rows-1;

  GTK_CLIST(pview->clist)->focus_row=sel;
  gtk_clist_select_row(GTK_CLIST(pview->clist),sel,-1);

  if (sel>=0) {
    pi = (PartitionInfo *)gtk_clist_get_row_data(GTK_CLIST(pview->clist),sel);
    pview->sel=pi;
    pgraph_select_pi(PGRAPH(pview->pgraph),pi);
  }
}

static void
clist_select_cb(PView *pview, gint sel)
{
  PartitionInfo *pi;

  pi=(PartitionInfo *)gtk_clist_get_row_data(GTK_CLIST(pview->clist),sel);
  pview->sel=pi;
  pgraph_select_pi(PGRAPH(pview->pgraph),pi);
}

static void 
pgraph_select_cb(PView *pview)
{
  int row;
  PartitionInfo *pi;

  pi=((PGraph *)(pview->pgraph))->sel;
  row=gtk_clist_find_row_from_data(GTK_CLIST(pview->clist),pi);
  clist_select_row(pview,row);
}

static void
pview_init (PView *pview)
{
  int i;
  GtkWidget *sw;

  pview->info=NULL;
  pview->show_extended=0;
  pview->sel=NULL;
  pview->unit=UNIT_CYLINDERS;

  pview->label = gtk_label_new("");
  gtk_box_pack_start (GTK_BOX(pview),pview->label,0,0,0);
  gtk_widget_show(pview->label);

  pview->spgraph = spgraph_new ();
  pview->pgraph = SPGRAPH(pview->spgraph)->pgraph;
  gtk_box_pack_start (GTK_BOX(pview),pview->spgraph,0,0,0);
  gtk_widget_show (pview->spgraph);

  gtk_signal_connect_object(GTK_OBJECT(pview->pgraph),"select_partition",
			    GTK_SIGNAL_FUNC(pgraph_select_cb),
			    (gpointer)pview);
			    
  for (i = 0; i < CLC_LAST; i++) {
    cl_names[i] = g_strconcat(" ",_(cl_names[i]),NULL);
  }

  pview->clist = gtk_clist_new_with_titles(CLC_LAST,cl_names);
  gtk_widget_show (pview->clist);

  sw = gtk_scrolled_window_new(NULL,NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  gtk_container_add(GTK_CONTAINER(sw),pview->clist);
  gtk_widget_show(sw);

  gtk_box_pack_start (GTK_BOX(pview),sw,1,1,0);

  gtk_clist_column_titles_passive(GTK_CLIST(pview->clist));

  gtk_clist_set_column_justification(GTK_CLIST(pview->clist),CLC_NUM,
				     GTK_JUSTIFY_LEFT);
  gtk_clist_set_column_justification(GTK_CLIST(pview->clist),CLC_ACTIVE,
				     GTK_JUSTIFY_CENTER);

  
  for (i=0;i<CLC_LAST;i++) { 
    gtk_clist_set_column_auto_resize(GTK_CLIST(pview->clist),i,TRUE);
    gtk_clist_set_column_min_width(GTK_CLIST(pview->clist),i,60);
  }
  gtk_clist_set_column_min_width(GTK_CLIST(pview->clist),CLC_SYSTEM,120);
  gtk_clist_set_column_min_width(GTK_CLIST(pview->clist),CLC_ID,35);
  gtk_clist_set_column_min_width(GTK_CLIST(pview->clist),CLC_OFFSET,35);

  gtk_clist_set_selection_mode(GTK_CLIST(pview->clist),GTK_SELECTION_BROWSE);

  gtk_signal_connect_object(GTK_OBJECT(pview->clist),"select_row",
		     GTK_SIGNAL_FUNC(clist_select_cb),
		     (gpointer)pview);

}

GtkWidget*
pview_new ()
{
  return GTK_WIDGET ( gtk_type_new (pview_get_type ()));
}

static char*
l2str(long l)
{
  char *r=malloc(50);
  sprintf(r," %ld ",l);
  return r;
}


static char*
free_usage(DevInfo *info,PartitionInfo *pi)
{
  switch (get_pusage(info,pi)) {
  case PRI: return(_("Free/P"));
  case LOG: return(_("Free/L"));
  case PRILOG: return(_("Free/PL"));
  case UNUSABLE: return(_("Unusable"));
  default: return("---");
  }
}

long 
unit(long sect, DevInfo *di, UnitType u)
{
  long cylsize;

  if (u==UNIT_CYLINDERS) {
    cylsize=di->heads*di->sectors;
    return(sect/cylsize);
  } else {
    return sect;
  }
}

static void 
add_row(PView *pview,PartitionInfo *pi) 
{
  int i,row;
  char *cl_entry[CLC_LAST];

  for(i=0;i<CLC_LAST;i++) cl_entry[i]=NULL;
  if (pi->ptype==FREESPACE) {
    cl_entry[CLC_NUM]=strdup(free_usage(pview->info,pi));
  } else {
    cl_entry[CLC_NUM]=l2str(pi->pnum+1);
    if (pi->boot_ind==0x80) cl_entry[CLC_ACTIVE]=strdup(_("Yes"));
    cl_entry[CLC_ID]=l2str(pi->sys_ind);
    cl_entry[CLC_OFFSET]=g_strconcat(" ",offset_str(pview->info,pi),
				     " ",NULL);
    cl_entry[CLC_SYSTEM]=strdup(get_sys_name(pi->sys_ind));
  }
  cl_entry[CLC_START]=l2str(unit(pi->first_sector,pview->info,pview->unit));
  cl_entry[CLC_END]=l2str(unit(pi->last_sector,pview->info,pview->unit));
  cl_entry[CLC_SIZE]=l2str((pi->last_sector-pi->first_sector)/2048);
  row = gtk_clist_append(GTK_CLIST(pview->clist),cl_entry);
  gtk_clist_set_row_data(GTK_CLIST(pview->clist),row,
			 (gpointer)pi);
  for(i=0;i<CLC_LAST;i++) if (cl_entry[i]) free(cl_entry[i]);

}

void 
pview_show_partition(PView *pview)
{
  PartitionInfo *pi,*lpi;
  int sel;
  long size;
  char buf[100],buf1[100],*t;

  size = (pview->info->heads*pview->info->sectors*pview->info->cylinders)/2048;
  t = (pview->info->htype==H_IDE) ? "IDE" : "SCSI";

  sprintf(buf1,"%s (%s/%d/%d) - %dMB",pview->info->model,t,pview->info->host,
	  pview->info->did,size);

  sprintf(buf,"%s",buf1);
  
  if (pview->info->changed) {
    strcat(buf," (");
    strcat(buf,_("modified"));
    strcat(buf,")");
  } else {
  }

  gtk_label_set(GTK_LABEL(pview->label),buf);

  if (GTK_CLIST(pview->clist)->selection) {
    sel = (int)GTK_CLIST(pview->clist)->selection->data;
  } else {
    sel=0;
  }
  gtk_clist_freeze(GTK_CLIST(pview->clist)); 
  gtk_clist_clear(GTK_CLIST(pview->clist));

  pi = pview->info->plist;
  while (pi) {
    if (pi->ptype==EXTENDED) {
      if (pview->show_extended) 
	add_row(pview,pi);
      lpi=pi->lplist;
      while (lpi) {
	add_row(pview,lpi);
	lpi = lpi->next;
      }
    } else {
      add_row(pview,pi);
    }
    pi = pi->next;
  }
  
  clist_select_row(pview,sel);
  gtk_clist_thaw(GTK_CLIST(pview->clist));

  pgraph_set_info(PGRAPH(pview->pgraph),pview->info);
}


void           
pview_set_info(PView *pview, DevInfo *di)
{
  PGraph *pgraph;

  g_return_if_fail (pview != NULL);
  g_return_if_fail (pview->pgraph != NULL);
  g_return_if_fail (IS_PVIEW (pview));

  pview->info=di;

  pgraph=(PGraph *) pview->pgraph;
  pgraph_set_info(pgraph,di);
  clist_select_row(pview,0);
  pview_show_partition(pview);
}

void
pview_show_extended(PView *pview,int b)
{
  g_return_if_fail (pview != NULL);
  g_return_if_fail (IS_PVIEW (pview));
  
  if (b!=pview->show_extended) {
    pview->show_extended=b;
    pview_show_partition(pview);
  }
}

void
pview_set_unit(PView *pview, UnitType u)
{
  g_return_if_fail (pview != NULL);
  g_return_if_fail (IS_PVIEW (pview));

  pview->unit=u;
  pview_show_partition(pview);
}













