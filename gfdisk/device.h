#ifndef DEVICE_H
#define DEVICE_H

#include <glib.h>
#include "part.h"

long rounded(DevInfo *info, long calcul, long start);
long hsc2lin(DevInfo *info, int h, int s, int c);
long next_cyl(DevInfo *info,long sec); 
long prev_cyl(DevInfo *info,long sec);

int read_sector(int fd, char *buffer, int sect_num);
int write_sector(int fd, char *buffer, int sect_num);
DevInfo *read_device_info(char *device);
GSList *get_devices();

#endif
