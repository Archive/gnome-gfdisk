#include <math.h>
#include <stdio.h>
#include <config.h>
#include <gnome.h>
#include <gtk/gtkmain.h>
#include "pstat.h"

static void pstat_init (PStat *pstat);
static void pstat_class_init (PStatClass *class);
static void pstat_destroy (GtkObject *object);
static void pstat_realize (GtkWidget *widget);
static void pstat_size_request (GtkWidget      *widget,
				GtkRequisition *requisition);
static gint pstat_expose (GtkWidget      *widget,
			  GdkEventExpose *event);
static void pstat_size_allocate (GtkWidget     *widget,
				 GtkAllocation *allocation);

static GtkWidgetClass *parent_class = NULL;

guint
pstat_get_type ()
{
  static guint pstat_type = 0;

  if (!pstat_type)
    {
      GtkTypeInfo pstat_info =
      {
	"PStat",
	sizeof (PStat),
	sizeof (PStatClass),
	(GtkClassInitFunc) pstat_class_init,
	(GtkObjectInitFunc) pstat_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL,
      };

      pstat_type = gtk_type_unique (gtk_widget_get_type (), &pstat_info);
    }

  return pstat_type;
}

static void
pstat_class_init (PStatClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (gtk_widget_get_type ());

  object_class->destroy = pstat_destroy;
  widget_class->realize = pstat_realize;
  widget_class->expose_event = pstat_expose;
  widget_class->size_request = pstat_size_request;
  widget_class->size_allocate = pstat_size_allocate;
}

static void
pstat_init (PStat *pstat)
{
}

GtkWidget*
pstat_new ()
{
  PStat *pstat;

  pstat = gtk_type_new (pstat_get_type ());

  return GTK_WIDGET (pstat);
}

static void
pstat_destroy (GtkObject *object)
{
  PStat *pstat;

  g_return_if_fail (object != NULL);
  g_return_if_fail (IS_PSTAT (object));

  pstat = PSTAT (object);
 

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);

  if (pstat->gc) gdk_gc_destroy(pstat->gc);
}

static void
pstat_realize (GtkWidget *widget)
{
  PStat *pstat;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PSTAT (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  pstat = PSTAT (widget);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events (widget) | 
    GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
    GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
    GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new (widget->parent->window, 
				   &attributes, attributes_mask);

  widget->style = gtk_style_attach (widget->style, widget->window);
  gdk_window_set_back_pixmap(widget->window, NULL, TRUE);

  gdk_window_set_user_data (widget->window, widget);

  pstat->gc=gdk_gc_new(widget->window);
  
  pstat->co[0].red=65535;
  pstat->co[0].green=0;
  pstat->co[0].blue=0;
  gdk_color_alloc(attributes.colormap,&pstat->co[0]);

  pstat->co[1].red=0x1861;
  pstat->co[1].green=0x9248;
  pstat->co[1].blue=0xffff;
  gdk_color_alloc(attributes.colormap,&pstat->co[1]);

  pstat->co[2].red=0;
  pstat->co[2].green=0;
  pstat->co[2].blue=0xffff;
  gdk_color_alloc(attributes.colormap,&pstat->co[2]);

}

static gint
pstat_min_width(GtkWidget *widget)
{
  gint h,w = 0;

  h=widget->style->font->ascent+widget->style->font->descent;
  w+=gdk_string_width(widget->style->font,_("Primary"));
  w+=gdk_string_width(widget->style->font,_("Logical"));
  w+=gdk_string_width(widget->style->font,_("Extended"));
  w+=2+3*(h+4)+2*8+GNOME_PAD;
  return(w);

}

static void 
pstat_size_request (GtkWidget      *widget,
		    GtkRequisition *requisition)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PSTAT (widget));

  requisition->width = pstat_min_width(widget);
  requisition->height = widget->style->font->ascent+
    widget->style->font->descent+8;
}

static void
pstat_size_allocate (GtkWidget     *widget,
		     GtkAllocation *allocation)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_PSTAT (widget));
  g_return_if_fail (allocation != NULL);
  
  widget->allocation = *allocation;
  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x, allocation->y,
			      allocation->width, allocation->height);
      gdk_window_set_back_pixmap(widget->window, NULL, TRUE);
      gdk_window_clear(widget->window);
      gtk_widget_draw(widget,NULL);
    }
}

static gint
pstat_expose (GtkWidget *widget, GdkEventExpose *event)
{
  PStat *pstat;
  char *buf;
  gint len,h,x,y;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (IS_PSTAT (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  pstat = PSTAT (widget);

  if (event->count > 0)
    return FALSE;

  if (GTK_WIDGET_DRAWABLE (widget)) {
    h=widget->style->font->ascent+widget->style->font->descent;
    y=h+4-widget->style->font->descent; 
    x=2;

    gdk_gc_set_foreground(pstat->gc,&pstat->co[2]);
    gdk_draw_rectangle (widget->window,
			pstat->gc,1,
			x,4,
			h,h);


    x+=h+4;
    buf = _("Primary");
    len=strlen(buf);
    gdk_draw_text(widget->window,widget->style->font,
		  widget->style->black_gc,
		  x,y,buf,len);

    x+=gdk_string_width(widget->style->font,buf)+8;


    gdk_gc_set_foreground(pstat->gc,&pstat->co[2]);
    gdk_draw_rectangle (widget->window,
			pstat->gc,1,
			x,4,
			h,h/2);
    gdk_gc_set_foreground(pstat->gc,&pstat->co[1]);
    gdk_draw_rectangle (widget->window,
			pstat->gc,1,
			x,4+h/2,
			h,h/2);


    x+=h+4;
    buf = _("Logical");
    len=strlen(buf);
    gdk_draw_text(widget->window,widget->style->font,
		  widget->style->black_gc,
		  x,y,buf,len);

    x+=gdk_string_width(widget->style->font,buf)+8;

    gdk_gc_set_foreground(pstat->gc,&pstat->co[2]);
    gdk_draw_rectangle (widget->window,
			pstat->gc,1,
			x,4,
			h,h/2);
    gdk_draw_rectangle (widget->window,
			widget->style->white_gc,1,
			x,4+h/2,
			h,h/2);


    x+=h+4;
    buf = _("Extended");
    len=strlen(buf);
    gdk_draw_text(widget->window,widget->style->font,
		  widget->style->black_gc,
		  x,y,buf,len);

  }

  return FALSE;
}

