#include <config.h>
#include <gnome.h>
#include <string.h>

#include "gfdisk.h"
#include "device.h"
#include "pgraph.h"
#include "pview.h"
#include "elist.h"
#include "pstat.h"
#include "etexts.h"

#ifndef ALLOW_WRITE
#define ALLOW_WRITE "none"
#endif

MyApplication app;

static GnomeUIInfo toolbar[] = {
  GNOMEUIINFO_ITEM_STOCK(N_("Reload"),
			 N_("Reload the partition table from the harddisk"),
			 reload_cb,GNOME_STOCK_MENU_OPEN),
  GNOMEUIINFO_ITEM_STOCK(N_("Save"),
			 N_("Save the partition table to the harddisk"),
			 partition_save_cb,GNOME_STOCK_MENU_SAVE),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK(N_("Create"),
			 N_("Create a new partition"),
			 partition_create_cb,GNOME_STOCK_MENU_PASTE),
  GNOMEUIINFO_ITEM_STOCK(N_("Delete"),
			 N_("Delete the selected partition"),
			 partition_delete_cb,GNOME_STOCK_MENU_CUT),
  GNOMEUIINFO_END
};


static void
destroy()
{
    gtk_main_quit();
}

static void
set_status(gint changed)
{
  if (changed) {
     gnome_appbar_set_default(GNOME_APPBAR(app.stat),
			      _("Partition Table has been changed"));
  } else {
     gnome_appbar_set_default(GNOME_APPBAR(app.stat),"");
  }
}

/* Session Management stuff */

static int
save_state (GnomeClient        *client,
	    gint                phase,
	    GnomeRestartStyle   save_style,
	    gint                shutdown,
	    GnomeInteractStyle  interact_style,
	    gint                fast,
	    gpointer            client_data)
{
  gchar *argv[20];
  gchar *s;
  gint i = 0, j;
  gint xpos, ypos;
  gint xsize, ysize;
  gchar geo[100];

  if (app.main) {
    gdk_window_get_origin(app.main->window, &xpos, &ypos);
    gdk_window_get_size(app.main->window, &xsize, &ysize);

    argv[i++] = program_invocation_name;

    argv[i++] = (char *) "--geometry";
    snprintf(geo,100,"%dx%d+%d+%d",xsize,ysize,xpos,ypos);
    argv[i++] = geo;

    gnome_client_set_restart_command (client, i, argv);
    gnome_client_set_clone_command (client, 0, NULL);
  }

  return TRUE;
} 

static GnomeClient
*newGnomeClient()
{
  gchar *buf[1024];
  
  GnomeClient *client;
  
  client = gnome_master_client();
  
  if (!client)
    return NULL;
  
  getcwd((char *)buf,sizeof(buf));
  gnome_client_set_current_directory(client, (char *)buf);
  
  gtk_object_ref(GTK_OBJECT(client));
  gtk_object_sink(GTK_OBJECT(client));
  
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
		      GTK_SIGNAL_FUNC (save_state), NULL);
  gtk_signal_connect (GTK_OBJECT (client), "die",
		      GTK_SIGNAL_FUNC (destroy), NULL);

  return client;
}

gint exit_cb(GtkWidget *widget, gpointer data)
{
  GtkWidget *d;
  GSList *t;
  DevInfo *info;
  char changed = 0;
  char buf[100];

  if (g_slist_length(app.devlist)>1) {  
    t = app.devlist->next;
    while (t){
      info= t->data;
      changed |= info->changed;
      t=t->next;
    }
    if (changed) {
      esprintf(buf,"I-MODIFIED");
      d = gnome_message_box_new(buf,
				GNOME_MESSAGE_BOX_QUESTION,
				GNOME_STOCK_BUTTON_YES,
				GNOME_STOCK_BUTTON_CANCEL,
				GNOME_STOCK_BUTTON_HELP,
				NULL);

      gtk_window_set_modal(GTK_WINDOW(d),TRUE);
      gtk_window_position(GTK_WINDOW(d), GTK_WIN_POS_MOUSE);

      gnome_dialog_set_close(GNOME_DIALOG(d),FALSE);

      gnome_dialog_button_connect(GNOME_DIALOG(d),0,
				  GTK_SIGNAL_FUNC(destroy),
				  NULL);

      gnome_dialog_button_connect_object(GNOME_DIALOG(d),1,
				  GTK_SIGNAL_FUNC(gnome_dialog_close),
				  (gpointer)d);
			      
      gnome_dialog_button_connect(GNOME_DIALOG(d),2,
				  GTK_SIGNAL_FUNC(ehelp_goto),
				  (gpointer)"I-MODIFIED");
			      
      gtk_signal_connect(GTK_OBJECT(d),"close",
			 GTK_SIGNAL_FUNC(gtk_widget_destroy),
			 NULL);

      gtk_widget_show(d);
    } else {
      destroy();
    }
  } else {
    destroy();
  }
}

void update_pview()
{
  if (app.automin) {
    minimize_ext(PVIEW(app.pview)->info);    
  }
  pview_show_partition(PVIEW(app.pview));
}


void no_hds()
{
  GtkWidget *d;
  char buf[200];

  esprintf(buf,"E-NODEV");
  d = gnome_message_box_new(buf,
			    GNOME_MESSAGE_BOX_ERROR,
			    GNOME_STOCK_BUTTON_OK,
			    GNOME_STOCK_BUTTON_HELP,
			    NULL);

  gnome_dialog_set_close(GNOME_DIALOG(d),FALSE);

  gnome_dialog_button_connect(GNOME_DIALOG(d),0,
			      GTK_SIGNAL_FUNC(destroy),
			      NULL);
			      
  gnome_dialog_button_connect(GNOME_DIALOG(d),1,
			      GTK_SIGNAL_FUNC(ehelp_goto),
			      (gpointer)"E-NODEV");
			      
  gtk_signal_connect(GTK_OBJECT(d),"close",
		     GTK_SIGNAL_FUNC(destroy),
		     NULL);

  gtk_widget_show(d);
}

#ifdef HAVE_LIBGNOMEPRINT
void print_cb()
{
  print_info(PVIEW(app.pview)->info);
  //  eview("I-NOT_IMPLEMENTED");
}
#endif

static void add_devinfo(GtkWidget *cl, char *c, char *v)
{
  char *cl_entry[2];

  cl_entry[0]=c;
  cl_entry[1]=v;

  gtk_clist_append(GTK_CLIST(cl),cl_entry);
}

void devinfo_cb (GtkWidget *widget, void *data)
{
  GtkWidget *d;
  GtkWidget *b,*t,*cl;
  gchar * titles[] = { _("Category"), _("Value") };
  char buf[100];
  DevInfo *info;
  GtkStyle * style;
  GdkFont * font;

  info = PVIEW(app.pview)->info;

  d=gnome_dialog_new(_("Device Information"),
		     GNOME_STOCK_BUTTON_CLOSE,
		     GNOME_STOCK_BUTTON_HELP,
		     NULL);
  gnome_dialog_set_close(GNOME_DIALOG(d), FALSE);

  gnome_dialog_button_connect(GNOME_DIALOG(d),1,
			      GTK_SIGNAL_FUNC(ehelp_goto),
			      (gpointer)"DEVINFO");
			      
  gnome_dialog_button_connect_object(GNOME_DIALOG(d),0,
			      GTK_SIGNAL_FUNC(gnome_dialog_close),
			      (gpointer)d);
			      
  gtk_signal_connect(GTK_OBJECT(d),"close",
		     GTK_SIGNAL_FUNC(gtk_widget_destroy),
		     NULL);

  style = gtk_style_new ();
  font = gdk_font_load ("-Adobe-Helvetica-Medium-R-Normal--*-160-*-*-*-*-*-*");

  if (font) {
    gdk_font_unref (style->font);
    style->font = font;
  }

  if (style->font) {
    gtk_widget_push_style (style);
  }

  t=gtk_label_new(info->model);

  if (style->font) {
    gtk_widget_pop_style();
  }
  else gtk_style_unref(style);


  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox),t,0,0,GNOME_PAD);

  cl = gtk_clist_new_with_titles(2,titles);

  gtk_clist_set_column_auto_resize(GTK_CLIST(cl),0,TRUE);
  gtk_clist_set_column_auto_resize(GTK_CLIST(cl),1,TRUE);
  gtk_clist_set_column_min_width(GTK_CLIST(cl),0,110);
  gtk_clist_set_column_min_width(GTK_CLIST(cl),1,110);

  gtk_clist_set_shadow_type(GTK_CLIST(cl),GTK_SHADOW_OUT);

  gtk_clist_set_selection_mode(GTK_CLIST(cl), GTK_SELECTION_BROWSE);

  gtk_clist_column_titles_passive(GTK_CLIST(cl));

  gtk_clist_freeze(GTK_CLIST(cl)); 

  add_devinfo(cl,_("Filesystem Path"),info->path);

  sprintf(buf,"%d MB",(info->heads*info->sectors*info->cylinders)/2048);
  add_devinfo(cl,_("Capacity"),buf);

  (info->htype == H_SCSI) ? add_devinfo(cl,_("Host Type"),"SCSI") :
    add_devinfo(cl,_("Host Type"),"IDE");

  sprintf(buf,"%d",info->host);
  add_devinfo(cl,_("Controller Number"),buf);

  sprintf(buf,"%d",info->did);
  add_devinfo(cl,_("Device ID"),buf);

  sprintf(buf,"%d",info->heads);
  add_devinfo(cl,_("Heads"),buf);
  sprintf(buf,"%d",info->sectors);
  add_devinfo(cl,_("Sectors"),buf);
  sprintf(buf,"%d",info->cylinders);
  add_devinfo(cl,_("Cylinders"),buf);

  sprintf(buf,"%d KB",(info->heads*info->sectors)/2);
  add_devinfo(cl,_("Cylinder Size"),buf);

  gtk_clist_thaw(GTK_CLIST(cl)); 

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox),cl,1,1,GNOME_PAD);

  gtk_widget_show_all(d);

}

void about_cb (GtkWidget *widget, void *data)
{
  GtkWidget *about;
  const gchar *authors[] = {
	  "Dietmar Maurer",
	  NULL
          };

  about = gnome_about_new ("Gnome FDisk",VERSION,
			   "Copyright (C) 1998 Dietmar Maurer",
			   authors,
			   _("This program is part of the GNOME project for "
			     "Linux. Gnome FDisk comes with ABSOLUTELY NO "
			     "WARRANTY. This is free software, and you are "
			     "welcome to redistribute it under the conditions "
			     "of the GNU General Public License. "
			     "Please report bugs to dm@vlsivie.tuwien.ac.at"),
			   NULL);

  gtk_widget_show (about);
}

gint set_units_cb(GtkWidget *widget, gpointer data)
{
  pview_set_unit(PVIEW(app.pview),(UnitType)data);
}

void toggle_mode_cb()
{
  if (app.show_ext) {
    app.show_ext=0;
    app.automin=1;
  } else {
    app.show_ext=1;
    app.automin=0;
  }
  pview_show_extended(PVIEW(app.pview),app.show_ext);
  update_pview();
}

void activate_cb()
{
  PartitionInfo *pi;

  pi=PVIEW(app.pview)->sel;

  if (pi && (pi->ptype!=FREESPACE)) {
    if (pi->boot_ind != 0x80) {
      inactivate_all_partitions(PVIEW(app.pview)->info);
      pi->boot_ind = 0x80;
      PVIEW(app.pview)->info->changed = 1;
      update_pview();
    } else {
      inactivate_all_partitions(PVIEW(app.pview)->info);
      PVIEW(app.pview)->info->changed = 1;
      update_pview();
    }
  } else {
    eview("I-NO_PART_SEL");
  }
  set_status(PVIEW(app.pview)->info->changed);
}

void maximize_cb()
{
  PartitionInfo *pi;

  pi=PVIEW(app.pview)->sel;

  if (pi && (pi->ptype!=FREESPACE)) {
      maximize_partition(PVIEW(app.pview)->info,pi);
      PVIEW(app.pview)->info->changed = 1;
      set_status(PVIEW(app.pview)->info->changed);
      update_pview();
  } else {
    eview("I-NO_PART_SEL");
  }
}

GtkWidget *create_pview(GSList *devlist, int cdev)
{
  DevInfo *info;

  info = (DevInfo *)(g_slist_nth(devlist,cdev)->data);

  app.pview=pview_new();
  gtk_container_border_width(GTK_CONTAINER(app.pview),GNOME_PAD);
  gtk_box_set_spacing(GTK_BOX(app.pview),GNOME_PAD);
  gtk_widget_show(app.pview);
  pview_set_info(PVIEW(app.pview),info);
  
  return(app.pview);
}

gint set_current_device(GtkWidget *widget, gpointer data)
{
  DevInfo *info;

  info = ((DevInfo *)(g_slist_nth(app.devlist,(int)data)->data));
  pview_set_info(PVIEW(app.pview),info);
  set_status(PVIEW(app.pview)->info->changed);
  update_pview();
}

gint partition_delete_cb(GtkWidget *widget, gpointer data)
{
  PartitionInfo *pi,*ext;
  DevInfo *info;

  pi=PVIEW(app.pview)->sel;
  info = PVIEW(app.pview)->info;
  ext = ext_part(info);

  if (pi && (pi->ptype==FREESPACE) && ext &&
      (ext->lplist==pi) && (ext->lplist->next==NULL)) {
      delete_part(info,ext);
      PVIEW(app.pview)->info->changed = 1;
      update_pview();    
  } else {
    if (pi && (pi->ptype!=FREESPACE)) {
      delete_part(info,pi);
      PVIEW(app.pview)->info->changed = 1;
      update_pview();
    } else {
      eview("I-NO_PART_SEL");
    }
  }
  set_status(PVIEW(app.pview)->info->changed);
}

struct pa_dialog {
  DevInfo *info;
  PType ptype;
  int fs,ls;
  GtkWidget *dc,*e,*d,*mi[5];
  GtkOptionMenu *pnm,*ptm;
  GtkAdjustment *a,*a1;
} pad;


gint partition_create(GtkWidget *widget, gpointer data)
{
  int fs,ls,cs,pnum,offset,dc;
  PartitionInfo *ext;

  cs=pad.info->heads*pad.info->sectors;

  fs = next_cyl(pad.info,(int)(pad.fs+pad.a1->value*cs));
  ls = prev_cyl(pad.info,(int)(fs+pad.a1->page_size*cs));

  dc = GTK_TOGGLE_BUTTON(pad.dc)->active; /* dos compatible ? */

  if (((ls-fs)>0)&&(ls>pad.fs)&&(ls<=pad.ls)) {
    if (pad.ptype==PRIMARY) {
      pnum = (int)gtk_object_get_user_data(GTK_OBJECT(pad.pnm->menu_item));
    } else {
      pnum = -1;
    }
    if (pad.ptype==LOGICAL) {
      expand_extended(pad.info,fs,ls);
      ext=ext_part(pad.info);
      if (dc) {
	offset = (fs == 0) ? ext->offset+pad.info->sectors : pad.info->sectors;
      } else {
	offset = (fs == 0) ? ext->offset+1 : 1;
      }
    } else {
      if (dc) {
	offset = (fs == 0) ? pad.info->sectors : 0;
      } else {
	offset = (fs == 0) ? 1 : 0;
      }
    }

    if (add_part(pad.info,pad.ptype,PT_LINUX,0,pnum,fs,ls,offset)) {
      pad.info->changed = 1;
      set_status(pad.info->changed);
      update_pview();
      gnome_dialog_close(GNOME_DIALOG(pad.d));
    } 
  }
}

void update_pad_entry()
{
  char buf[100];
  int cs;

  cs=pad.info->heads*pad.info->sectors;

  sprintf(buf,"%.2f MB",pad.a1->page_size*cs/2048.0);
  gtk_entry_set_text(GTK_ENTRY(pad.e),buf);
}

gint slider_update_cb(GtkAdjustment *adjustment, gpointer data)
{
  GtkAdjustment *a;

  a=GTK_ADJUSTMENT(data);
  a->page_size=adjustment->value;
  if ((a->value+a->page_size)>a->upper) a->value=a->upper-a->page_size;
  gtk_signal_emit_by_name(GTK_OBJECT(a), "changed");
  update_pad_entry();
}

gint pad_update_cb(GtkWidget *widget, gpointer data)
{
  int t,i;
  GtkWidget *m;

  t=(int)data;
  m=gtk_option_menu_get_menu(pad.pnm);

  if (t==0) { /* primary */
    pad.ptype=PRIMARY;
    for(i=0;i<4;i++) if (!pnum_in_use(pad.info,i)) {
      gtk_widget_set_sensitive(GTK_WIDGET(pad.mi[i]),TRUE);
      gtk_option_menu_set_history(pad.pnm,i);  
      break;
    }
    for(i=0;i<4;i++) {
      if (pnum_in_use(pad.info,i)) {
	gtk_widget_set_state(GTK_WIDGET(pad.mi[i]),GTK_STATE_INSENSITIVE);
      } else {
	gtk_widget_set_sensitive(GTK_WIDGET(pad.mi[i]),TRUE);
      }
    }
    gtk_widget_set_state(GTK_WIDGET(pad.mi[4]),GTK_STATE_INSENSITIVE);
  }

  if (t==1) { /* logical */
    pad.ptype=LOGICAL;
    gtk_widget_set_sensitive(GTK_WIDGET(pad.mi[4]),TRUE);
    gtk_option_menu_set_history(pad.pnm,4);  
    for(i=0;i<4;i++) {
      gtk_widget_set_state(GTK_WIDGET(pad.mi[i]),GTK_STATE_INSENSITIVE);
    }
  }

}

void draw_create_dialog(PartitionInfo *pi) 
{
  GtkWidget *t,*b,*om,*m,*mi,*table;
  GtkWidget *d;
  GtkWidget *scrollbar;
  GtkWidget *scale;
  GtkObject *adjustment,*adjustment1;
  int i;
  float maxsize,cs;
  PUsage us;
  char *title;

  us = get_pusage(pad.info,pi);

  title = _("Create Primary/Logical Partition"); // default
  if (us==PRI) title = _("Create Primary Partition");
  if (us==LOG) title = _("Create Logical Partition");

  d=gnome_dialog_new(title,_("Create"),
		     GNOME_STOCK_BUTTON_CANCEL,
		     GNOME_STOCK_BUTTON_HELP,
		     NULL);
  gtk_window_set_modal(GTK_WINDOW(d),TRUE);

  
  gtk_signal_connect(GTK_OBJECT (d), "close",
		     GTK_SIGNAL_FUNC(gtk_widget_destroy),
		     NULL);

  gnome_dialog_button_connect(GNOME_DIALOG(d),0,
			      GTK_SIGNAL_FUNC(partition_create),
			      NULL);

  gnome_dialog_button_connect_object(GNOME_DIALOG(d),1,
			      GTK_SIGNAL_FUNC(gnome_dialog_close),
			      (gpointer)d);

  gnome_dialog_button_connect(GNOME_DIALOG(d),2,
			      GTK_SIGNAL_FUNC(ehelp_goto),
			      (gpointer)"PADINFO");
			      
	
  pad.d = d;
  gtk_window_position(GTK_WINDOW(d), GTK_WIN_POS_MOUSE);
  {
    table = gtk_table_new(6,3,FALSE);
    {
      b=gtk_label_new(_("Size:"));
      gtk_label_set_justify(GTK_LABEL(b),GTK_JUSTIFY_LEFT);
      gtk_widget_show(b);
      gtk_table_attach_defaults(GTK_TABLE(table),b,0,1,0,1);
      
      pad.e=gtk_entry_new();
      gtk_entry_set_editable(GTK_ENTRY(pad.e),FALSE);
      gtk_widget_set_usize(pad.e,80,-1);
      gtk_widget_show(pad.e);
      gtk_table_attach_defaults(GTK_TABLE(table),pad.e,1,2,0,1);
      
      b=gtk_label_new("Partition #:");
      gtk_widget_show(b);
      gtk_table_attach_defaults(GTK_TABLE(table),b,2,3,0,1);

      om = gtk_option_menu_new();
      pad.pnm = GTK_OPTION_MENU(om);

      gtk_widget_show(om);
      m = gtk_menu_new();
      {
	char buf[5];

	for (i=1;i<5;i++) {
	  sprintf(buf,"%d",i);
	  mi = gtk_menu_item_new_with_label(buf);
	  gtk_object_set_user_data(GTK_OBJECT(mi),(gpointer)(i-1)); 
	  gtk_widget_show(mi);
	  gtk_menu_append(GTK_MENU(m),mi);
	  pad.mi[i-1]=mi;
	}
       
	mi = gtk_menu_item_new_with_label(_("Auto"));
	gtk_object_set_user_data(GTK_OBJECT(mi),(gpointer)(-1)); 
	gtk_widget_show(mi);
	gtk_menu_append(GTK_MENU(m),mi);
	pad.mi[4]=mi;
      }
      gtk_option_menu_set_menu(GTK_OPTION_MENU(om),m);
      gtk_table_attach_defaults(GTK_TABLE(table),om,3,4,0,1);

      om = gtk_option_menu_new();
      pad.ptm=GTK_OPTION_MENU(om);

      gtk_widget_show(om);
      m = gtk_menu_new();
      {
	if ((us==PRI)||(us==PRILOG)) {
	  mi = gtk_menu_item_new_with_label(_("Primary"));
	  gtk_object_set_user_data(GTK_OBJECT(mi),(gpointer)(0)); 
	  gtk_widget_show(mi);
	  gtk_menu_append(GTK_MENU(m),mi);
	  gtk_signal_connect(GTK_OBJECT (mi), "activate",
			     GTK_SIGNAL_FUNC(pad_update_cb),
			     NULL);
	}
	if ((us==LOG)||(us==PRILOG)) {
	  mi = gtk_menu_item_new_with_label(_("Logical"));
	  gtk_object_set_user_data(GTK_OBJECT(mi),(gpointer)(1)); 
	  gtk_widget_show(mi);
	  gtk_menu_append(GTK_MENU(m),mi);
	  gtk_signal_connect(GTK_OBJECT (mi), "activate",
			     GTK_SIGNAL_FUNC(pad_update_cb),
			     (gpointer)1);
	}   
      }
      gtk_option_menu_set_menu(GTK_OPTION_MENU(om),m);
      gtk_table_attach_defaults(GTK_TABLE(table),om,4,5,0,1);

      if (us==LOG) {
	pad_update_cb(GTK_WIDGET(om),(gpointer)1);
      } else {
	pad_update_cb(GTK_WIDGET(om),NULL);
      }

      pad.dc = gtk_check_button_new_with_label(_("DC"));
      gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(pad.dc),1);
      gtk_widget_show(pad.dc);
      gtk_table_attach_defaults(GTK_TABLE(table),pad.dc,5,6,0,1);
    }
  
    cs=pad.info->heads*pad.info->sectors;
    maxsize=(prev_cyl(pad.info,pad.ls)-next_cyl(pad.info,pad.fs))/cs;

    {
      b=gtk_label_new(_("Size:"));
      gtk_label_set_justify(GTK_LABEL(b),GTK_JUSTIFY_LEFT);
      gtk_widget_show(b);
      gtk_table_attach_defaults(GTK_TABLE(table),b,0,1,1,2);

      adjustment = gtk_adjustment_new (maxsize,
				       1.0,maxsize+1.0,
				       1.0,1.0,1.0);
      pad.a = GTK_ADJUSTMENT(adjustment);
      scale = gtk_hscale_new (GTK_ADJUSTMENT (adjustment));
      gtk_widget_set_usize (GTK_WIDGET (scale), 350,-1);
      gtk_range_set_update_policy (GTK_RANGE (scale), GTK_UPDATE_CONTINUOUS);
      gtk_scale_set_digits (GTK_SCALE (scale),0);
      gtk_scale_set_draw_value (GTK_SCALE (scale),FALSE);
      gtk_table_attach_defaults(GTK_TABLE(table),scale,1,6,1,2);
      gtk_widget_show (scale);
    }

    {
      b=gtk_label_new(_("Position:"));
      gtk_label_set_justify(GTK_LABEL(b),GTK_JUSTIFY_LEFT);
      gtk_widget_show(b);
      gtk_table_attach_defaults(GTK_TABLE(table),b,0,1,2,3);

      adjustment1 = gtk_adjustment_new (0.0,0.0,maxsize,
					1.0,1.0,maxsize);
      pad.a1 = GTK_ADJUSTMENT(adjustment1);
      update_pad_entry();
      scrollbar = gtk_hscrollbar_new (GTK_ADJUSTMENT (adjustment1));
      gtk_widget_set_usize (GTK_WIDGET (scrollbar), 350,-1);
      gtk_range_set_update_policy (GTK_RANGE (scrollbar), 
				   GTK_UPDATE_CONTINUOUS);
      gtk_signal_connect(GTK_OBJECT (adjustment), "value_changed",
		     GTK_SIGNAL_FUNC(slider_update_cb),
		     (gpointer)adjustment1);
      gtk_table_attach_defaults(GTK_TABLE(table),scrollbar,1,6,2,3);
      gtk_widget_show (scrollbar);
      
    }
  }

  gtk_table_set_col_spacings(GTK_TABLE(table),6);
  gtk_table_set_row_spacing(GTK_TABLE(table),0,20);
  gtk_table_set_row_spacing(GTK_TABLE(table),1,20);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox),table,1,1,0);
  gtk_widget_show(table);
  gtk_widget_show(d);

}

gint partition_create_cb(GtkWidget *widget, gpointer data)
{
  int sec;
  PartitionInfo *pi;
  PUsage pu;

  pi=PVIEW(app.pview)->sel;

  if ((pi) && (pi->ptype == FREESPACE)) {
    pu = get_pusage(PVIEW(app.pview)->info,pi);
    pad.fs=pi->first_sector;
    pad.ls=pi->last_sector;
    pad.info=PVIEW(app.pview)->info;
    if (pu!=UNUSABLE) {
      if (pu==LOG) pad.ptype=LOGICAL;
      else pad.ptype=PRIMARY;
      draw_create_dialog(pi);
    } else {
      eview("E-MAX_PART");
    }
  } else {
    eview("E-NEED_FREE_SPACE");
  }
  update_pview();
}

void create_toolbar(GtkWidget *window, GSList *devlist)
{
  GtkWidget *b,*tb,*t,*m,*mi;
  GSList *sl;
  int i,j;

  tb = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL,
		       GTK_TOOLBAR_BOTH);
  {
    t = gtk_option_menu_new();
    gtk_widget_show(t);
    m = gtk_menu_new();

    j = g_slist_length(devlist);

    for (i=1;i<j;i++) {
      mi = gtk_menu_item_new_with_label(
	   ((DevInfo *)(g_slist_nth(devlist,i)->data))->path);
      gtk_widget_show(mi);
      gtk_menu_append(GTK_MENU(m),mi);
      gtk_signal_connect(GTK_OBJECT(mi),"activate",
			 GTK_SIGNAL_FUNC(set_current_device),(gpointer)i);
    }

    gtk_option_menu_set_menu(GTK_OPTION_MENU(t),m);

    gtk_toolbar_append_widget(GTK_TOOLBAR(tb),t,_("Select a device"),
			      NULL);

    gtk_toolbar_append_space(GTK_TOOLBAR(tb));

    gnome_app_fill_toolbar(GTK_TOOLBAR(tb),toolbar,NULL);
  }

  gnome_app_set_toolbar(GNOME_APP(window),
			GTK_TOOLBAR(tb));
}

void partition_save_cb(GtkWidget *widget, gpointer data)
{
  DevInfo *info;
  int changed;

  info = PVIEW(app.pview)->info;

  info->changed = 0; // always clear changed flag ?
  set_status(info->changed);

  if (!strcmp(ALLOW_WRITE,info->path)||!strcmp(ALLOW_WRITE,"all")) {
    if ((changed=write_pt(info))>=0) {
      delete_all_partitions(info);
      read_pt(info);
      if (!changed) {
	eview("I-NEED_REBOOT");
      } else {
	eview("I-WROTE_TABLE");
      }
    } else {
      eview("E-WRITE_FAILED");
    }
  } else {
    eview("E-WRITE_NOT_ENABLED%s",ALLOW_WRITE);
  }

  update_pview();
}

void reload_cb(GtkWidget *widget, gpointer data)
{
  delete_all_partitions(PVIEW(app.pview)->info);
  read_pt(PVIEW(app.pview)->info);
  PVIEW(app.pview)->info->changed = 0;
  set_status(PVIEW(app.pview)->info->changed);
  update_pview();
}

void set_sysind_cb(GtkWidget *widget, int sysind)
{
  DevInfo *info;
  PartitionInfo *pi;

  info = PVIEW(app.pview)->info;

  pi=PVIEW(app.pview)->sel;

  if ((pi) && (pi->ptype != FREESPACE)) {
    if (pi->sys_ind != sysind) {
      info->changed = 1;
      set_status(info->changed); 
      if (is_extended(pi->sys_ind)) {
	if (is_extended(sysind)) {
	  pi->sys_ind = sysind;
	} else {
	  delete_all_logical(info);
	  pi->sys_ind = sysind;
	  pi->ptype = PRIMARY;
	}
      } else {
	if (is_extended(sysind)) {
	  if (ext_part(info)==NULL) {
	    pi->sys_ind = sysind;
	    pi->ptype = EXTENDED;
	    pi->label = NULL;
	    pi->lplist = new_pinfo(FREESPACE,pi->first_sector,pi->last_sector);
	  } else {
	    eview("E-SECOND_EXT");
	  }
	} else {
	  pi->sys_ind = sysind;
	  if (!is_dos_partition(sysind)) pi->label=NULL;
	}
      }
      update_pview();
    }
  } else {
    eview("I-NO_PART_SEL");
  }
}

void delete_all_partitions_cb(GtkWidget *widget,gpointer data)
{
  delete_all_partitions(PVIEW(app.pview)->info);
  PVIEW(app.pview)->info->changed = 1;
  set_status(PVIEW(app.pview)->info->changed);
  update_pview();
}

static struct {
  char *defdev;
  int mode;
  char *geometry;
} cloptions;

struct poptOption prog_options[] = {
  {NULL, 'a', POPT_ARG_NONE, &cloptions.mode, 0, 
   N_("Advanced mode"), NULL},
  {"geometry", '\0', POPT_ARG_STRING, &cloptions.geometry, 0, 
   N_("Geometry for starting up"), N_("GEOMETRY")},
  POPT_AUTOHELP
  {NULL, '\0', 0, NULL}
  };

int main(int argc, char *argv[]) 
{
  GtkWidget *hb,*vb,*t;
  int xpos, ypos, width, height;
  poptContext ctx;

  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  app.automin = 0;
  app.show_ext = 0;


  gnome_init_with_popt_table("gfdisk", VERSION, argc, argv,
			     prog_options,0,&ctx);

  cloptions.defdev = poptGetArg(ctx);

  if (poptGetArg(ctx)) {
    printf("Error: too many arguments\n");
    exit(1);
  }

  newGnomeClient();
  edb_append(etexts);
  app.el = g_slist_alloc();

  app.devlist = get_devices(cloptions.defdev);

  if (g_slist_length(app.devlist)>1) {  
    app.main = gnome_app_new("gfdisk",_("Gnome FDisk"));
    gtk_window_set_wmclass(GTK_WINDOW(app.main), "gfdisk",
			   "gfdisk");

    gtk_signal_connect(GTK_OBJECT(app.main),"delete_event",
		       GTK_SIGNAL_FUNC(exit_cb),NULL);

    vb = gtk_vbox_new(0,0);
    {
      gtk_box_pack_start(GTK_BOX(vb),create_pview(app.devlist,1),1,1,0);

      t = gtk_hseparator_new();
      gtk_widget_show(t);
      gtk_box_pack_start(GTK_BOX(vb),t,0,0,0);
    }
    gtk_widget_show(vb);

    gnome_app_set_contents(GNOME_APP(app.main), vb);

    app.stat = gnome_appbar_new(FALSE,TRUE,GNOME_PREFERENCES_NEVER);
    gtk_widget_show(app.stat);
    gtk_container_border_width(GTK_CONTAINER(app.stat),GNOME_PAD);
    t = pstat_new();
    gtk_widget_show(t);
    gtk_box_pack_start(GTK_BOX(app.stat),t,0,0,0);
    gtk_box_reorder_child(GTK_BOX(app.stat),t,0);

    gnome_app_set_statusbar(GNOME_APP(app.main),app.stat);

    create_menus(app.main);
    create_toolbar(app.main,app.devlist);

    gtk_window_set_focus(GTK_WINDOW(app.main),PVIEW(app.pview)->clist);

    /* set normal mode as default */
    app.automin=1;
    app.show_ext=0;
    if (cloptions.mode==1) { /* set normal mode */
      set_advanced_mode();
    }
 
    gtk_widget_show(app.main);

    if (gnome_parse_geometry(cloptions.geometry,&xpos,&ypos,&width,&height)){
      if (xpos != -1) {
	gdk_window_move(GTK_WIDGET(app.main)->window,xpos,ypos);
      }
      if (width != -1) {
	gdk_window_resize(GTK_WIDGET(app.main)->window,width,height);
      }
    } else {
	gtk_window_set_default_size (GTK_WINDOW (app.main), 640, 480);
    }

    update_pview();
  } else {
    no_hds();
  }

  gtk_main();

  return(0);
}




