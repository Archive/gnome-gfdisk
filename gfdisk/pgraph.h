#ifndef __PGRAPH_H__
#define __PGRAPH_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include "part.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define PGRAPH(obj)          GTK_CHECK_CAST (obj, pgraph_get_type (),PGraph)
#define PGRAPH_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, pgraph_get_type (), PGraphClass)
#define IS_PGRAPH(obj)       GTK_CHECK_TYPE (obj, pgraph_get_type ())

typedef struct _PGraph        PGraph;
typedef struct _PGraphClass   PGraphClass;

struct _PGraph
{
  GtkWidget widget;

  PartitionInfo *sel;
  DevInfo *info;
  GdkGC *gc;
  GdkColor co[3];
  gint offset;
  gint pheight;
  gint pminx;
};

struct _PGraphClass
{
  GtkWidgetClass parent_class;

  void (*select_partition) (PGraph * clist, PartitionInfo *pi);

};


GtkWidget*     pgraph_new                    ();
guint          pgraph_get_type               (void);

void           pgraph_set_info               (PGraph *pgraph,
					      DevInfo *di);
gint           pgraph_select_pi(PGraph *pgraph, PartitionInfo *pi);
void           pgraph_set_offset              (PGraph *pgraph,
					       gint offset);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __PGRAPH_H__ */
