#ifndef __PSTAT_H__
#define __PSTAT_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define PSTAT(obj)          GTK_CHECK_CAST (obj, pstat_get_type (),PStat)
#define PSTAT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, pstat_get_type (), PStatClass)
#define IS_PSTAT(obj)       GTK_CHECK_TYPE (obj, pstat_get_type ())

typedef struct _PStat        PStat;
typedef struct _PStatClass   PStatClass;

struct _PStat
{
  GtkWidget widget;
  GdkGC *gc;
  GdkColor co[3];
};

struct _PStatClass
{
  GtkWidgetClass parent_class;
};


GtkWidget*     pstat_new                    ();
guint          pstat_get_type               (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __PSTAT_H__ */
