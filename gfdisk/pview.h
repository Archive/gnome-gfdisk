#ifndef __PVIEW_H__
#define __PVIEW_H__


#include <gdk/gdk.h>
#include <gtk/gtkvbox.h>
#include "part.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum
{
  UNIT_CYLINDERS,
  UNIT_SECTORS
} UnitType;


#define PVIEW(obj)          GTK_CHECK_CAST (obj, pview_get_type (), PView)
#define PVIEW_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, pview_get_type (), PViewClass)
#define IS_PVIEW(obj)       GTK_CHECK_TYPE (obj, pview_get_type ())


typedef struct _PView       PView;
typedef struct _PViewClass  PViewClass;

struct _PView
{
  GtkVBox vbox;

  GtkWidget *pgraph;
  GtkWidget *spgraph;
  GtkWidget *clist;
  GtkWidget *label;

  DevInfo *info;
  PartitionInfo *sel;

  int show_extended;
  UnitType unit;
};

struct _PViewClass
{
  GtkVBoxClass parent_class;

};

guint          pview_get_type        (void);
GtkWidget*     pview_new             (void);
void           pview_set_info        (PView *pview,DevInfo *di);
void           pview_show_extended   (PView *pview,int b);
void           pview_show_partition  (PView *pview);
void           pview_set_unit        (PView *pview,UnitType u);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __PVIEW_H__ */
