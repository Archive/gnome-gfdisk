#ifndef GFDISK_H
#define GFDISK_H

#include <config.h>
#include <gnome.h>
#include "elist.h"

typedef struct {
  int automin;
  int show_ext;
  GSList *devlist;
  GtkWidget *main;
  GtkWidget *pview;
  GtkWidget *stat;
  EList *el;
} MyApplication;

gint exit_cb(GtkWidget *widget, gpointer data);
void about_cb (GtkWidget *widget, void *data);
void activate_cb();
void devinfo_cb (GtkWidget *widget, void *data);
void maximize_cb();
gint partition_delete_cb(GtkWidget *widget, gpointer data);
gint partition_create_cb(GtkWidget *widget, gpointer data);
void set_sysind_cb(GtkWidget *widget, int sysind);
void delete_all_partitions_cb(GtkWidget *widget,gpointer data);
gint set_units_cb(GtkWidget *widget, gpointer data);
void partition_save_cb(GtkWidget *widget, gpointer data);
void reload_cb(GtkWidget *widget, gpointer data);
void toggle_mode_cb();
void print_cb();

#endif
