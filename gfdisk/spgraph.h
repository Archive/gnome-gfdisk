#ifndef __SPGRAPH_H__
#define __SPGRAPH_H__


#include <gdk/gdk.h>
#include <gtk/gtkvbox.h>
#include "pgraph.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SPGRAPH(obj)          GTK_CHECK_CAST (obj, spgraph_get_type (),SPGraph)
#define SPGRAPH_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, spgraph_get_type (), SPGraphClass)
#define IS_SPGRAPH(obj)       GTK_CHECK_TYPE (obj, spgraph_get_type ())


typedef struct _SPGraph       SPGraph;
typedef struct _SPGraphClass  SPGraphClass;

struct _SPGraph
{
  GtkVBox vbox;

  GtkWidget *pgraph;
  GtkAdjustment *adj;
  GtkWidget *sb;

  gint offset;
};

struct _SPGraphClass
{
  GtkVBoxClass parent_class;

};

guint          spgraph_get_type        (void);
GtkWidget*     spgraph_new             (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __SPGRAPH_H__ */
