#include <config.h>
#include <gnome.h>
#include <string.h>
#include <stdarg.h>
#include <libgnome/gnome-help.h>
#include <glib.h>

#include "elist.h"

typedef struct {
  char *name;
  char *text;
} EInfo;

#define MAXELENGTH 1000

static char *stdtexts[] = {
  "I-NO_HELP",N_("Sorry, no help available."),
  NULL,NULL
};

static GHashTable *ehash;

void 
edb_append(char ** texts) 
{
  static int initialize = 1;
  char *t;
  int i=0;
  int err;
  int warn = 0;

  if (initialize) {

    ehash = g_hash_table_new(g_str_hash,g_str_equal);
    initialize = 0;
    edb_append(stdtexts);
  }

  while (texts[i]) {
    err = 0;

    t=_(texts[i+1]);

    if (t) {
      int ia = -1;
      int ib = -1;

      do {
	ia++; ib++;
	while(texts[i][ia] &&
	      (texts[i][ia]!='%')) ia++;

	while(t[ib] &&
	      ((t[ib]!='%') ||
	       ((t[ib]=='%')&&(t[ib+1]=='%')))) {
	  if ((t[ib]=='%')&&(t[ib+1]=='%')) ib+=2;
	  else ib++;
	}

	if (texts[i][ia] && t[ib]) { 
	  if (texts[i][ia+1]!=t[ib+1]) { err=1; break; }
	} else {
	  if (texts[i][ia] || t[ib])  { err=1; break; }
	} 

      } while (texts[i][ia] && t[ib]);
	 
    } else err = 1;

    if (!err) g_hash_table_insert(ehash,texts[i],t);
    else g_hash_table_insert(ehash,texts[i],texts[i]);

    if (err) warn = 1;
    i+=2;
  }

  if (warn) {
    printf(_("Error during initialization of Error Database."));
    printf("\n");
  }
}

static char * 
elockup(char *name)
{
  char *t;
  int i = 0;

  g_return_val_if_fail(ehash!=NULL,NULL);

  return(g_hash_table_lookup(ehash,name));

}

static void
vesprintf(char *buf, char *name, va_list args)
{
  char *t;

  g_return_if_fail(buf!=NULL);
  g_return_if_fail(name!=NULL);

  if (!(t=elockup(name))) t=name;

  vsprintf(buf,t,args);
}

static void
veprintf(char *name, va_list args)
{
  char *t;

  g_return_if_fail(name!=NULL);

  if (!(t=elockup(name))) t=name;

  vprintf(t,args);
}

void
esprintf(char *buf, char *name, ...)
{
  va_list args;
  va_start (args,name);
  vesprintf(buf,name,args);
  va_end (args);
}

void
eprintf(char *name, ...)
{
  va_list args;
  va_start (args,name);
  veprintf(name,args);
  va_end (args);
}

void 
elist_append(EList *el, char *name, ...) 
{
  va_list args;
  char *t;
  EInfo *ei;
  char ebuf[MAXELENGTH];

  g_return_if_fail(el!=NULL);
  g_return_if_fail(name!=NULL);

  va_start (args,name);
  vesprintf(ebuf,name,args);
  va_end (args);

  ei = (EInfo *)malloc(sizeof(EInfo));
  ei->name = strdup(name);
  ei->text = strdup(ebuf);
  g_slist_append(el,(gpointer)ei);
}

void
elist_clear(EList *el) 
{
  GSList *l;
  EInfo *ei;

  g_return_if_fail(el!=NULL);

  l=el->next;

  while (l) {
    ei = (EInfo *)l->data;
    free(ei->name);
    free(ei->text);
    free(ei);
    l = g_slist_next(l);
  }
  g_slist_free(el->next);
  el->next=NULL;
}

static void 
add_einfo(GtkWidget *cl, EInfo *ei)
{
  char *cl_entry[2];
  int row;

  cl_entry[0]=_("Information");

  if ((tolower(ei->name[0])=='e')&&(ei->name[1]=='-')) {
    cl_entry[0]=_("Error");
  }
  if ((tolower(ei->name[0])=='w')&&(ei->name[1]=='-')) {
    cl_entry[0]=_("Warning");
  }

  cl_entry[1]=ei->text;
  row = gtk_clist_append(GTK_CLIST(cl),cl_entry);
  gtk_clist_set_row_data(GTK_CLIST(cl),row,
			 (gpointer)strdup(ei->name));
}

void
ehelp_goto(void *ignore, char *topic)
{
  gchar *buf,*t;
  char *htopic;
  int i,l;

  htopic=strdup(topic);

  l = strlen(htopic);
  for(i=0;i<l;i++) if (!isalpha(htopic[i])) htopic[i]='-';
  else htopic[i]=toupper(htopic[i]);

  t = gnome_help_file_path("gfdisk","gfdisk-help.html");
  buf=g_copy_strings("file://",t,"#",htopic, NULL);

  gnome_help_goto(NULL,buf);

  free(htopic);
  g_free(buf);
}

static gint 
elist_cb(GtkWidget *widget,char *htopic)
{
  char *t;
  GtkWidget *cl;
  int sel;

  cl = (GtkWidget *)gtk_object_get_data(GTK_OBJECT(widget),"CLIST");
 
  if (cl) {
    sel = (int)GTK_CLIST(cl)->selection->data;
    t = (char *)gtk_clist_get_row_data(GTK_CLIST(cl),sel);
  } else {
    t=htopic;
  }
  
  if (t) {
    ehelp_goto(NULL,t);
  } else {
    eview("I-NO_HELP");
  }
}

gint 
elist_count(EList *el) 
{
  g_return_val_if_fail((el!=NULL),0);

  return(g_slist_length(el)-1);
}

void 
va_eview_list(EList *el, char *name, va_list args) 
{
  GtkWidget *d,*cl,*t,*sw;
  gchar * titles[] = { g_strconcat(" ",_("Type"),NULL), 
		       g_strconcat(" ",_("Description"),NULL) };
  GSList *l;
  EInfo *ei;
  char *text;
  char *htopic;
  char *dtype;
  char ebuf[MAXELENGTH];

  g_return_if_fail(name!=NULL);

  dtype = GNOME_MESSAGE_BOX_ERROR;

  if (!(text=elockup(name))) {
    text = name;
    htopic = NULL;
  } else {
    htopic = strdup(name);
    if (!el) {
      if ((tolower(name[0])=='e')&&(name[1]=='-')) {
	dtype = GNOME_MESSAGE_BOX_ERROR;
      }
      if ((tolower(name[0])=='w')&&(name[1]=='-')) {
	dtype = GNOME_MESSAGE_BOX_WARNING;
      }
      if ((tolower(name[0])=='i')&&(name[1]=='-')) {
	dtype = GNOME_MESSAGE_BOX_INFO;
      }
    }
  }

  vsprintf(ebuf,text,args);

  d = gnome_message_box_new(ebuf,dtype,
			    GNOME_STOCK_BUTTON_CLOSE,
			    GNOME_STOCK_BUTTON_HELP,
			    NULL);

  gtk_window_position(GTK_WINDOW(d), GTK_WIN_POS_MOUSE);
  gnome_dialog_set_close(GNOME_DIALOG(d),FALSE);

  
  gnome_dialog_button_connect(GNOME_DIALOG(d),1,
			      GTK_SIGNAL_FUNC(elist_cb),
			      (gpointer)htopic);
			      
  gnome_dialog_button_connect_object(GNOME_DIALOG(d),0,
			      GTK_SIGNAL_FUNC(gnome_dialog_close),
			      (gpointer)d);
			      
  gtk_signal_connect(GTK_OBJECT(d),"close",
		     GTK_SIGNAL_FUNC(gtk_widget_destroy),
		     (gpointer)0);
		     
		     

  if (el && elist_count(el)) {

    t = ((GtkBoxChild  *)(GTK_BOX(GNOME_DIALOG(d)->vbox)->children->data))
      ->widget;

    gtk_box_set_child_packing(GTK_BOX(GNOME_DIALOG(d)->vbox),
			      t,0,1,0,GTK_PACK_START);
			      
    gtk_window_set_policy (GTK_WINDOW (d),FALSE, TRUE, FALSE);
    gtk_widget_set_usize(GTK_WIDGET(d),440,320);

    cl = gtk_clist_new_with_titles(2,titles);
    gtk_object_set_data(GTK_OBJECT(d),"CLIST",(gpointer)cl);

    gtk_clist_set_column_width(GTK_CLIST(cl),0,70);

    gtk_clist_set_selection_mode(GTK_CLIST(cl), GTK_SELECTION_BROWSE);

    gtk_clist_column_titles_passive(GTK_CLIST(cl));
    
    /*
    gtk_clist_set_policy (GTK_CLIST(cl),
			  GTK_POLICY_AUTOMATIC,
			  GTK_POLICY_AUTOMATIC);
			  */		  
    gtk_clist_freeze(GTK_CLIST(cl)); 

    l=el->next;
    while (l) {
      ei = (EInfo *)l->data;
      add_einfo(cl,ei);
      l = g_slist_next(l);
    }

    gtk_clist_thaw(GTK_CLIST(cl)); 

    sw = gtk_scrolled_window_new(NULL,NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
				   GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
    gtk_container_add(GTK_CONTAINER(sw),cl);
    gtk_widget_show(sw);

    gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox),sw,1,1,GNOME_PAD);

  }

  gtk_widget_show_all(d);
}

void 
eview(char *name, ...)
{
  va_list args;
 
  va_start (args,name);
  va_eview_list(NULL, name, args); 
  va_end (args);
}

void 
eview_list(EList *el, char *name, ...)
{ 
  va_list args;
 
  va_start (args,name);
  va_eview_list(el, name, args); 
  va_end (args);
}
