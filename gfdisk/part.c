#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/fs.h>
#include <string.h>
#include <gnome.h>

#include "device.h"
#include "part.h"


#define set_hsc(h,s,c,sector) \
{ \
      s = sector % sectors + 1;	\
      sector /= sectors;	\
      h = sector % heads;	\
      sector /= heads;		\
      c = sector & 0xFF;	\
      s |= (sector >> 2) & 0xC0;\
}

#define PART_TABLE_FLAG 0xAA55
#define MAXIMUM_PARTITIONS 256
#define NUM_PART_TYPES 256
static char *partition_type[NUM_PART_TYPES] = {
    [PT_LINUX_MINIX]          = N_("Linux/MINIX"),
    [PT_LINUX_SWAP]           = N_("Linux Swap"),
    [PT_LINUX]                = N_("Linux"),
    [PT_FREE_SPACE]           = N_("Free Space"),
    [PT_DOS_EXTENDED]         = N_("Extended"),
    [PT_LINUX_EXTENDED]       = N_("Linux extended"),
    [PT_DOS_FAT12]            = N_("DOS FAT12"),
    [PT_DOS_FAT16]            = N_("DOS FAT16"),
    [PT_DOS_FAT16_BIG]        = N_("DOS FAT16 (big)"),
    [PT_OS2_HPFS]             = N_("OS/2 HPFS or NTFS"),
    [PT_OS2_BOOT]             = N_("OS/2 Boot Manager"),
    [PT_WIN95_FAT32]          = N_("Win95 FAT32"),
    [PT_WIN95_FAT32_LBA]      = N_("Win95 FAT32 (LBA)"),
    [PT_WIN95_FAT16_LBA]      = N_("Win95 FAT16 (LBA)"),
    [PT_WIN95_EXT_LBA]        = N_("Win95 Extended (LBA)"),
    [PT_DOS_HIDDEN_FAT12]     = N_("Hidden DOS FAT12"),
    [PT_DOS_HIDDEN_FAT16]     = N_("Hidden DOS FAT16"),
    [PT_DOS_HIDDEN_FAT16_BIG] = N_("Hidden DOS FAT16 (big)"),
    [PT_BSD_386]              = N_("BSD/386"),
    [PT_XENIX_ROOT]           = N_("XENIX root"),
    [PT_XENIX_USR]            = N_("XENIX usr"),
    [PT_AIX]                  = N_("AIX"),
    [PT_AIX_BOOT]             = N_("AIX bootable"),
    [PT_VENIX]                = N_("Venix 80286"),
    [PT_NOVEL]                = N_("Novell?"),
    [PT_MICROPORT]            = N_("Microport"),
    [PT_GNU_HURD]             = N_("GNU HURD"),
    [PT_NOVEL_NETWARE_286]    = N_("Novell Netware 286"),
    [PT_NOVEL_NETWARE_386]    = N_("Novell Netware 386"),
    [PT_PC_IX]                = N_("PC/IX"),
    [PT_MINIX_OLD]            = N_("Old MINIX"),
    [PT_AMOEBA]               = N_("Amoeba"),
    [PT_AMOEBA_BBT]           = N_("Amoeba BBT"),
    [PT_BSDI_FS]              = N_("BSDI fs"),
    [PT_BSDI_SWAP]            = N_("BSDI swap"),
    [PT_SYRINX]               = N_("Syrinx"),
    [PT_CPM]                  = N_("CP/M"),
    [PT_DOS_ACCESS]           = N_("DOS access"),
    [PT_DOS_RO]               = N_("DOS R/O"),
    [PT_DOS_SECONDARY]        = N_("DOS secondary"),
    [PT_BBT]                  = N_("BBT")
};

char *get_sys_name(unsigned char sys_ind)
{

  if (partition_type[sys_ind]==NULL) {
    return(_("Unknown"));
  } else {
    return(_(partition_type[sys_ind]));
  }
}

char *
offset_str(DevInfo *info,PartitionInfo *pi)
{
  char buf[30];

  if (pi->offset == info->sectors) {
    sprintf(buf,"DC(%d)",pi->offset);
    return(strdup(buf));
  }
  if ((pi->offset == 0)&& (pi->pnum<4)) return(strdup("DC(0)"));
  else {
    sprintf(buf,"%d",pi->offset);
    return(strdup(buf));
  }
}

PartitionInfo *new_pinfo(PType ptype, int fs, int ls) 
{
  PartitionInfo *tmp;

  tmp = (PartitionInfo *)calloc(1,sizeof(PartitionInfo));
  tmp->ptype=ptype;
  tmp->first_sector=fs;
  tmp->last_sector=ls;

  return(tmp);
}

PartitionInfo * plist_split(PartitionInfo *pi,int sec)
{
  PartitionInfo *tmp;

  if (sec<=pi->first_sector) return(pi);
  if (sec>=pi->last_sector) return(pi->next);

  tmp = new_pinfo(FREESPACE,sec,pi->last_sector);
  tmp->next=pi->next;
  tmp->prev=pi;
  if (pi->next) pi->next->prev=tmp;
  pi->next=tmp;
  pi->last_sector=sec;

  return(tmp);
}

PartitionInfo * plist_insert(DevInfo *info, PartitionInfo *pi) {
  PartitionInfo *tmp,*h,*ext,*p,*n;

  tmp=NULL;

  if ((pi->ptype==PRIMARY)||(pi->ptype==EXTENDED)) {
    tmp=info->plist;
  } else {
    if ((ext=ext_part(info))&&(pi->ptype==LOGICAL)) {
      tmp=ext->lplist;
    }
  }

  while(tmp && !((tmp->ptype==FREESPACE)&&
	  (pi->first_sector>=tmp->first_sector)&&
	  (pi->last_sector<=tmp->last_sector))) tmp = tmp->next;

  if (tmp) {
    h=plist_split(tmp,pi->first_sector);
    plist_split(h,pi->last_sector);
    p=h->prev;n=h->next;
    *h=*pi;
    h->prev=p;h->next=n;
    free(pi);
    return(h);
  }
  return(NULL);
}

PartitionInfo *ext_part(DevInfo *info) {
  PartitionInfo *h;

  h=info->plist;
  while(h && !(h->ptype==EXTENDED)) h=h->next;
  return(h);
}

int is_ext_pi(DevInfo *info,PartitionInfo *pi) {
  PartitionInfo *tmp;

  if (tmp=ext_part(info)) {
    tmp=tmp->lplist;
    while(tmp && (tmp!=pi)) tmp=tmp->next;
    if (tmp) return(1); else return(0);
  } else {
    return(0);
  }
}

int pnum_in_use(DevInfo *info,int pnum) {
  PartitionInfo *tmp;

  tmp=info->plist;
  while(tmp) {
    if ((tmp->ptype!=FREESPACE)&&(tmp->pnum==pnum)) return(1);
    tmp=tmp->next;
  }
  return(0);
}

int primary_count(DevInfo *info) {
  int i=0;
  PartitionInfo *tmp=info->plist;
  while(tmp) {
    if (tmp->ptype!=FREESPACE) i++;
    tmp=tmp->next;
  }
  return(i);
}

int enumerate_logical(DevInfo *info)
{
  PartitionInfo *h,*ext;
  int lnum = 4;

  if (ext=ext_part(info)) {
    h=ext->lplist;
    while (h) {
      if (h->ptype==LOGICAL) {
	h->pnum=lnum;
	++lnum;
      }
      h = h->next;
    }
  }
  return(lnum-4);
}

void maximize_partition(DevInfo *info, PartitionInfo *pi) {
  PartitionInfo *tmp,*ext,*prev,*next;

  prev = next = NULL;
  ext = ext_part(info);

  if (pi->prev && (pi->prev->ptype == FREESPACE)) prev = pi->prev;
  if (pi->next && (pi->next->ptype == FREESPACE)) next = pi->next;

  if (prev) {
    pi->first_sector=prev->first_sector;
    pi->prev=prev->prev;
    if (prev->prev) prev->prev->next=pi;

    if (prev->first_sector==0) {
      pi->offset = (pi->ptype==LOGICAL) ? 
	ext->offset+info->sectors : info->sectors;
    } else {
      pi->offset = (pi->ptype==LOGICAL) ? info->sectors : 0;
    }

    if (info->plist==prev) info->plist=pi;
 
    if (pi->ptype==EXTENDED) {
      tmp = ext->lplist;
      if (tmp->ptype==FREESPACE) {
	tmp->first_sector=prev->first_sector;
	free(prev);
      } else {
	prev->prev=NULL;
	prev->next=ext->lplist;
	ext->lplist->prev=prev;
	ext->lplist=prev;
      }
    } else {
      if (ext && (ext->lplist==prev)) ext->lplist=pi;
      free(prev);
    }
  }

  if (next) {
    pi->last_sector=next->last_sector;
    pi->next=next->next;
    if (next->next) next->next->prev=pi;
    if (pi->ptype==EXTENDED) {
      tmp =ext->lplist;
      while (tmp->next) tmp=tmp->next;
      if (tmp->ptype==FREESPACE) {
	tmp->last_sector=next->last_sector;
	free(next);
      } else {
	next->next=NULL;
	next->prev=tmp;
	tmp->next=next;
      }
    } else {
      free(next);
    }
  }
}

int inactivate_all_partitions(DevInfo *info)
{
  PartitionInfo *h,*ext;

  h=info->plist;
  while (h) {
    h->boot_ind = 0;
    h = h->next;
  }
  if (ext=ext_part(info)) {
    h=ext->lplist;
    while (h) {
      h->boot_ind = 0;
      h = h->next;
    }
  }
  return(1);
}

int delete_all_logical(DevInfo *info) 
{
  PartitionInfo *h,*h1,*ext;

  if (ext=ext_part(info)) {
    h = ext->lplist;
    ext->lplist = NULL;
    while (h) {
      h1 = h->next;
      free(h);
      h = h1;
    }    
  }
  return(1);
}

int delete_part(DevInfo *info,PartitionInfo *pi) {
  PartitionInfo *h,*h1;

  if (pi->ptype!=FREESPACE) {
    if (pi->ptype==EXTENDED) delete_all_logical(info);
    pi->ptype = FREESPACE;
    pi->sys_ind = 0;
    pi->boot_ind = 0;
    pi->pnum = -1;
    pi->offset = 0;
    pi->lplist = NULL;

    h = pi;
    if ((pi->prev)&&(pi->prev->ptype==FREESPACE)) {
      h = pi->prev;
      pi->prev->last_sector = pi->last_sector;
      pi->prev->next = pi->next;
      if (pi->prev->next) {
	pi->prev->next->prev = pi->prev;
      }
      free(pi);
    }
    if ((h->next)&&(h->next->ptype==FREESPACE)) {
      h1 = h->next;
      h->last_sector = h->next->last_sector;
      h->next = h->next->next;
      if (h->next) {
	h->next->prev = h;
      }
      free(h1);
    }
  }

  enumerate_logical(info);
  return(1);
}

int delete_all_partitions(DevInfo *info)
{
  PartitionInfo *h,*h1;

  delete_all_logical(info);
  h=info->plist;
  while (h) {
    h1=h->next;
    free(h);
    h = h1;
  }
  info->plist = new_pinfo(FREESPACE,0,
			  info->heads*info->sectors*info->cylinders);
  return(1);
}


void minimize_ext(DevInfo *info) 
{
  PartitionInfo *tmp,*ext,*first,*last;

  if (ext=ext_part(info)) {
    first=last=ext->lplist;
    while(last->next) last=last->next;

    if (first==last) {
      if (first->ptype==FREESPACE) {
	delete_part(info,ext);
	return;
      } else {
	return;
      }
    }

    if (first->ptype==FREESPACE) {
      first->next->prev=NULL;
      ext->lplist=first->next;
      ext->first_sector=first->last_sector;
      ext->offset=0;
      if (!(ext->prev) || (ext->prev->ptype!=FREESPACE)) {
	first->next=ext;
	first->prev=ext->prev;
	if (ext->prev) ext->prev->next=first;
	ext->prev=first;
	if (info->plist==ext) info->plist=first;
      } else {
	ext->prev->last_sector=first->last_sector;
	free(first);
      }
    }

    if (last->ptype==FREESPACE) {
      last->prev->next=NULL;
      ext->last_sector=last->first_sector;
      if (!(ext->next) || (ext->next->ptype!=FREESPACE)) {
	last->next=ext->next;
	last->prev=ext;
	if (ext->next) ext->next->prev=last;
	ext->next=last;
      } else {
	ext->next->first_sector=last->first_sector;
	free(last);
      }
    }
  }
}

PartitionInfo * add_part(DevInfo *info, PType ptype, unsigned char sys_ind, 
	     unsigned char boot_ind, 
	     int pnum, int fs, int ls, int offset) 
{
  PartitionInfo *pi,*tmp;
  pi = new_pinfo(ptype,fs,ls);
  pi->sys_ind = sys_ind;
  pi->boot_ind = boot_ind;
  pi->pnum = pnum;
  pi->offset = offset;
  if (ptype==EXTENDED) {
    pi->lplist = new_pinfo(FREESPACE,fs,ls);
  }
  tmp=plist_insert(info,pi);
  enumerate_logical(info);
  return(tmp);
}

int expand_extended(DevInfo *info,int fs,int ls)
{
  PartitionInfo *ext,*tmp,*h;
  int pnum;

  /* NOTE: creates allways dos compatibe extended partitions */

  ext=ext_part(info);
  pnum=0;while((pnum<4)&&(pnum_in_use(info,pnum))) pnum++;
  if (!ext) {
    if (pnum<4) {
      add_part(info,EXTENDED,PT_DOS_EXTENDED,0,pnum,fs,ls,
	       (fs==0) ? info->sectors : 0);
      return(1);
    } else {
      return(0);
    }
  } else {
    if ((fs>=ext->first_sector)&&(ls<=ext->last_sector)) return(1);

    if (fs<ext->first_sector) {
      if (!(tmp=ext->prev) || (tmp->ptype!=FREESPACE)) return(0);
      if (tmp->first_sector<=fs) {
	h=plist_split(tmp,fs);
	if (h->prev) h->prev->next=ext;
	else info->plist=ext;
	ext->prev=h->prev;
	ext->first_sector=fs;
	if (fs==0) ext->offset=info->sectors; else ext->offset=0;
	if (ext->lplist->ptype==FREESPACE) {
	  ext->lplist->first_sector=fs;
	  free(h);
	} else {
	  h->prev=NULL;
	  h->next=ext->lplist;
	  ext->lplist->prev=h;
	  ext->lplist=h;
	}
	return(1);
      } else {
	return(0);
      }
    }

    if (ls>ext->last_sector) {
      if (!(tmp=ext->next) || (tmp->ptype!=FREESPACE)) return(0);
      if (tmp->last_sector>=ls) {
	plist_split(tmp,ls);
	if (tmp->next) tmp->next->prev=ext;
	ext->next=tmp->next;
	ext->last_sector=ls;
	h=ext->lplist;while(h->next) h=h->next;
	if (h->ptype==FREESPACE) {
	  h->last_sector=ls;
	  free(tmp);
	} else {
	  tmp->prev=h;
	  tmp->next=NULL;
	  h->next=tmp;
	}
	return(1);
      } else {
	return(0);
      }
    }

  }
}

char * get_dos_label(int fd, int start)
{
  int i;
  char label[12];
  PartitionTable pt;

  if (!read_sector(fd,pt.c.b,start)) {perror("ERR");return(NULL);}

  for (i=0;i<11;i++) {
    if(!isascii(pt.c.b[43+i])) {label[i] = 0;break;}
    else label[i]=pt.c.b[43+i];
  }
  label[11]=0;
  if (i>0) return(strdup(label));
  else return(strdup("NO NAME"));
}

int read_pt(DevInfo *info)
{
  int i,j,fd,p,start,logical_num,logical_sector,lstart,loff,loff1;
  PartitionTable pt,lpt;
  PartitionInfo *pi;

  if  ((fd=open(info->path,O_RDONLY))==-1) return(0);

  info->plist = new_pinfo(FREESPACE,0,
			  info->heads*info->sectors*info->cylinders);

  if (!read_sector(fd,pt.c.b,0)) {
    close(fd);
    return(0);
  }
     
  if (pt.p.flag != PART_TABLE_FLAG) {
    close(fd);
    printf("Unknown Partition Table\n");
    return(1);
  }

  for(i=0;i<4;i++) {
    if (pt.p.part[i].sys_ind>0) {
      if(is_extended(pt.p.part[i].sys_ind)) {
	if (ext_part(info) == NULL) {
	  start = pt.p.part[i].start_sect;
	  add_part(info,EXTENDED,pt.p.part[i].sys_ind,pt.p.part[i].boot_ind,i,
		   ((start<=info->sectors) ? 0 : start),
		   start+pt.p.part[i].nr_sects,
		   ((start<=info->sectors) ? start : 0));

	  logical_sector = pt.p.part[i].start_sect;
	  logical_num = 0;
	  if (read_sector(fd,lpt.c.b,logical_sector) &&
	      (lpt.p.flag == 0xAA55)) {

	    do {

	      p=0;
	      while ((p<4)&&((!lpt.p.part[p].sys_ind || 
			      is_extended(lpt.p.part[p].sys_ind)))) 
		p++;

	      if (p<4) {
		lstart=prev_cyl(info,logical_sector+lpt.p.part[p].start_sect);
		loff=(logical_sector+lpt.p.part[p].start_sect)-lstart;
		if (pi=add_part(info,LOGICAL,lpt.p.part[p].sys_ind,
			     lpt.p.part[p].boot_ind,-1,
			     lstart,
			     logical_sector+lpt.p.part[p].start_sect+ 
			     lpt.p.part[p].nr_sects,loff)) {
		  logical_num++;
		  if (is_dos_partition(pi->sys_ind)) {
		    pi->label=get_dos_label(fd,pi->first_sector+pi->offset);
		  }
		}
	      }

	      p=0;
	      while ((p<4)&&(!is_extended(lpt.p.part[p].sys_ind))) p++;
	      
	      if (p<4) {
		logical_sector = pt.p.part[i].start_sect +
		  lpt.p.part[p].start_sect;
		if (!((read_sector(fd,lpt.c.b,logical_sector) &&
		       (lpt.p.flag == 0xAA55)))) p=4;
	      }

	    } while ((p<4) & (logical_num < (MAXIMUM_PARTITIONS-4)));
	  } else {
	    printf("EXTENDED READ FAILED\n");
	  }
	} else {
	    printf("Second EXTENDED\n");
	}
      } else {
	start = pt.p.part[i].start_sect;
	if (pi=add_part(info,PRIMARY,pt.p.part[i].sys_ind,
			pt.p.part[i].boot_ind,i,
			((start<=info->sectors) ? 0 : start),
			start+pt.p.part[i].nr_sects,
			((start<=info->sectors) ? start : 0))) {
	  if (is_dos_partition(pi->sys_ind)) {
	    pi->label=get_dos_label(fd,pi->first_sector+pi->offset);
	  }
	}
      }
    }
  }
  close(fd);
  return(1);
}

void fill_part_table(DevInfo *info, struct partition *p, 
		     PartitionInfo *pi, int efs)
{
    int sects,heads,sectors;

    heads=info->heads;
    sectors=info->sectors;

    p->boot_ind = pi->boot_ind;
    p->sys_ind = pi->sys_ind;
    if (pi->ptype==LOGICAL)
      p->start_sect = pi->first_sector - efs + pi->offset;
    else 
      p->start_sect = pi->first_sector + pi->offset;
    p->nr_sects = pi->last_sector - (pi->first_sector+pi->offset);
    sects=(((pi->first_sector+pi->offset)/(info->heads*info->sectors)>1023) ?
	     info->heads*info->sectors*1024-1 : pi->first_sector+pi->offset);
    set_hsc(p->head, p->sector, p->cyl, sects);
    sects = ((pi->last_sector/(info->heads*info->sectors)>1023) ?
	     info->heads*info->sectors*1024-1 : pi->last_sector-1);
    set_hsc(p->end_head, p->end_sector, p->end_cyl, sects);
}

void fill_primary_table(DevInfo *info,PartitionTable *buffer)
{
    int i;
    PartitionInfo *pi;

    for (i=0x1BE;i<SECTOR_SIZE;i++) buffer->c.b[i] = 0;

    pi=info->plist;
    while(pi) {
      if (pi->ptype!=FREESPACE)
	fill_part_table(info,&(buffer->p.part[pi->pnum]),pi,0);
      pi=pi->next;
    }

    buffer->p.flag = PART_TABLE_FLAG;
}

int fill_logical_table(DevInfo *info,PartitionTable *buffer,
		       PartitionInfo *pi,int efs)
{
  int i,heads,sectors,sects,ns;
  PartitionInfo *next_pi,*ext;
  struct partition *p;

  heads=info->heads;
  sectors=info->sectors;

  ext=ext_part(info);

  if (buffer->p.flag != (unsigned short)PART_TABLE_FLAG)
    for (i = 0; i < SECTOR_SIZE; i++) buffer->c.b[i]=0;

  for (i=0x1BE;i<SECTOR_SIZE;i++) buffer->c.b[i]=0;

  ns = 0;

  if (pi) {
    
    fill_part_table(info,&(buffer->p.part[0]),pi,efs);

    next_pi=pi->next;
    while (next_pi && next_pi->ptype!=LOGICAL) next_pi=next_pi->next;

    if (next_pi) {
      p = &(buffer->p.part[1]);
      ns = next_pi->first_sector;
      p->boot_ind = 0;
      p->sys_ind = PT_DOS_EXTENDED;
      p->start_sect = next_pi->first_sector-(ext->first_sector+ext->offset);
      p->nr_sects = next_pi->last_sector - next_pi->first_sector;
      sects = ((next_pi->first_sector/(sectors*heads) > 1023) ?
	       heads*sectors*1024 - 1 : next_pi->first_sector);
      set_hsc(p->head, p->sector, p->cyl, sects);
      sects = ((next_pi->last_sector/(sectors*heads) > 1023) ?
	       heads*sectors*1024 - 1 : next_pi->last_sector-1);
      set_hsc(p->end_head, p->end_sector, p->end_cyl, sects);

    }
  }

  buffer->p.flag = PART_TABLE_FLAG;
  return(ns);
}

int write_pt(DevInfo *info) {
  int fd,ext_fs,ns,changed;
  PartitionTable buffer;
  PartitionInfo *pi,*ext;

  if  ((fd=open(info->path,O_RDWR))!=-1) {

    read_sector(fd,buffer.c.b, 0);
    fill_primary_table(info,&buffer);
    write_sector(fd,buffer.c.b, 0);

    if (ext=ext_part(info)) {
      pi=ext->lplist;
      ext_fs=ext->first_sector+ext->offset;
      if ((pi->ptype==FREESPACE)&&(pi->next==NULL)) {
	    read_sector(fd,buffer.c.b,ext_fs);
	    fill_logical_table(info,&buffer,NULL,ext_fs);
	    write_sector(fd,buffer.c.b,ext_fs);
      } else {
	while (pi) {
	  if (pi->ptype==LOGICAL) {
	    read_sector(fd,buffer.c.b,ext_fs);
	    ns=fill_logical_table(info,&buffer,pi,ext_fs);
	    write_sector(fd,buffer.c.b,ext_fs);
	    ext_fs=ns;
	  }
	  pi=pi->next;
	}
      }
    }
    sync();
    if (!ioctl(fd,BLKRRPART)) changed = 1; else changed = 0;
    sync();
    close(fd);
    return(changed);
  } else {
    return(-1);
  }
}

PUsage get_pusage(DevInfo *info,PartitionInfo *pi)
{
  PUsage us;
  PartitionInfo *ext,*last;
  int pc,lc;

  
  ext=ext_part(info);
  pc = primary_count(info);
  lc = enumerate_logical(info);

  if (pi->ptype!=FREESPACE) return(USED);

  if (is_ext_pi(info,pi)) {
    if (lc<(max_partitions(info)-4)) return(LOG);
    else return(UNUSABLE);
  }

  if (ext) {
    last=ext->lplist;
    while (last->next) last=last->next;

    if ((pi->last_sector==ext->lplist->first_sector) || 
	(pi->first_sector==last->last_sector)) {
      if ((pc<4)&&(lc<(max_partitions(info)-4))) return(PRILOG); 
      if (pc<4) return(PRI); 
      if (lc<(max_partitions(info)-4)) return(LOG); 
      return(UNUSABLE);
    } else {
      if (pc<4) return(PRI);
      else return(UNUSABLE);
    }
  } else {
    if ((pc<4)&&(lc<(max_partitions(info)-4))) return(PRILOG); 
    if (pc<4) return(PRI); 
    return(UNUSABLE);
  }

}









