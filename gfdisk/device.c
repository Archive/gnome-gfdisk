#include <stdio.h>
#include <glob.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/major.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <scsi/scsi.h>
#include <scsi/scsi_ioctl.h>
#include <string.h>
#include <linux/hdreg.h>
#include <linux/genhd.h>
#include <sys/sysmacros.h>

#include "device.h"

#define MAXIMUM_IDE_PARTITIONS 64
#define MAXIMUM_SCSI_PARTITIONS 16

#define sector(s)	 ((s) & 0x3f)
#define cylinder(s, c)	 ((c) | (((s) & 0xc0) << 2))

int max_partitions(DevInfo *info)
{
  switch (info->htype) {
  case H_SCSI:return(MAXIMUM_SCSI_PARTITIONS);
  case H_IDE:return(MAXIMUM_IDE_PARTITIONS);
  }
}

long rounded(DevInfo *info, long calcul, long start)
{
	long i;
	long max = 1024*info->heads*info->sectors;

	while ((i=calcul+max) <= start)
		calcul = i;
	return calcul;
}

long hsc2lin(DevInfo *info, int h, int s, int c) 
{
  return(sector(s)-1+info->sectors*(h+info->heads*cylinder(s,c)));

}

long next_cyl(DevInfo *info,long sec) 
{
  long cs;

  cs = info->heads*info->sectors;

  if ((sec%cs)!=0) return((1+sec/cs)*cs);
  else return((sec/cs)*cs);
}

long prev_cyl(DevInfo *info,long sec) 
{
  long cs;
  cs = info->heads*info->sectors;
  return((sec/cs)*cs);
}

int read_sector(int fd, char *buffer, int sect_num)
{
       
    if (lseek64 (fd, ((__off64_t) sect_num)*SECTOR_SIZE, SEEK_SET) < 0)
      return 0;
    if (read(fd, buffer, SECTOR_SIZE) != SECTOR_SIZE)
      return 0;
    return 1;
}

int write_sector(int fd, char *buffer, int sect_num)
{
    if (lseek64 (fd, ((__off64_t) sect_num)*SECTOR_SIZE, SEEK_SET) < 0)
	return 0;
    if (write(fd, buffer, SECTOR_SIZE) != SECTOR_SIZE)
	return 0;
    return 1;
}

static char * 
strip_name(char *n)
{
  int i,p,l;

  l=strlen(n);
  p=i=0;

  while (i<l) {
    if ((n[i]!=' ')||((n[i]==' ')&&(n[i+1]!=' '))) {
      n[p]=n[i];
      p++;
    }
    i++;
  }
  n[p]=0;
  return(strdup(n)); 
}

DevInfo *read_device_info(char *device)
{
  struct stat fi;
  struct hd_driveid hdi;
  struct hd_geometry geometry;
  int i,fd,major;
  unsigned char buffer[96];
  unsigned char *cmd;
  unsigned char idlun[8];
  DevInfo *di;
  PartitionTable pt;

  if (!device) return NULL;

  lstat(device,&fi);
  major = major(fi.st_rdev);
  if (!((major==IDE0_MAJOR)||(major==IDE1_MAJOR)||(major==IDE2_MAJOR)||
      (major==IDE3_MAJOR)||(SCSI_DISK_MAJOR(major)))) 
    return NULL;

  if (!S_ISBLK(fi.st_mode)) return NULL;
  if  ((fd=open(device,O_RDONLY))<0) return NULL;

  if (((major==IDE0_MAJOR)||(major==IDE1_MAJOR) ||
       (major==IDE2_MAJOR)||(major==IDE3_MAJOR)) &&
      ((fi.st_rdev&0x3f)==0)) {
    if ((!ioctl(fd,HDIO_GET_IDENTITY,&hdi)) &&
	(!ioctl(fd, HDIO_GETGEO, &geometry))) {

      di = (DevInfo *)malloc(sizeof(DevInfo));

      di->changed = 0;
      di->model = strip_name(hdi.model);
      di->path = strdup(device);
      di->heads = geometry.heads;
      di->sectors = geometry.sectors;
      di->cylinders = geometry.cylinders;
      di->host = 
	(major==IDE1_MAJOR)+(major==IDE2_MAJOR)*2+(major==IDE3_MAJOR)*3;
      di->did = (fi.st_rdev&0x40)>>6;
      di->htype = H_IDE;


      if (!di->heads||!di->sectors||!di->cylinders) {
	free(di);
	close(fd);
	return NULL;
      }

      if (read_pt(di)) {
	close(fd);
	return di;
      } else {
	free(di);
	close(fd);
	return NULL;
      }
    } else {
      close(fd);
      return NULL;
    }
  }
 
  if ((SCSI_DISK_MAJOR(major)) && ((fi.st_rdev&0x0f)==0)) {
    for (i=0;i<96;i++) buffer[i]=0;

    *((int *) buffer) = 0;	        /* length of input data */
    *(((int *) buffer) + 1) = 96;	/* length of output buffer */

    cmd = (char *) (((int *) buffer) + 2);

    cmd[0] = 0x12;		/* INQUIRY */
    cmd[1] = 0x00;		/* lun=0, evpd=0 */
    cmd[2] = 0x00;		/* page code = 0 */
    cmd[3] = 0x00;		/* (reserved) */
    cmd[4] = 96;		/* allocation length */
    cmd[5] = 0x00;		/* control */

    if ((!ioctl(fd,SCSI_IOCTL_SEND_COMMAND,buffer)) &&
	(!ioctl(fd,SCSI_IOCTL_GET_IDLUN,idlun)) &&
	(!ioctl(fd,HDIO_GETGEO,&geometry))) {

      buffer[40]=0;

      di = (DevInfo *)malloc(sizeof(DevInfo));

      di->changed = 0;
      di->model = strip_name(buffer+16);
      di->path = strdup(device);
      di->heads = geometry.heads;
      di->sectors = geometry.sectors;
      di->cylinders = geometry.cylinders;
      di->host = *((unsigned long *)(idlun+4));
      di->did = idlun[0];
      di->htype = H_SCSI;

      if (!di->heads||!di->sectors||!di->cylinders) {
	free(di);
	close(fd);
	return NULL;
      }

      if (read_pt(di)) {
	close(fd);
	return di;
      } else {
	free(di);
	close(fd);
	return NULL;
      }
    } else {
      close(fd);return NULL;
    }
  }

  close(fd);
  return NULL;

}

GSList *get_devices(char *adev)
{
  GSList *devlist=g_slist_alloc();
  int i;
  glob_t dev;
  DevInfo *info;

  if (!adev) {
    glob("/dev/*",0,NULL,&dev);
    for (i=0;i<dev.gl_pathc;i++) {
      if ((info=read_device_info(dev.gl_pathv[i]))!=NULL) {
	g_slist_append(devlist,info);
      }
    }
    globfree(&dev);
  } else {
    if ((info=read_device_info(adev))!=NULL) {
      g_slist_append(devlist,info);
    }
  }

  return devlist;
}
