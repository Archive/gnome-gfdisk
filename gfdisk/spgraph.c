#include <config.h>
#include <gnome.h>

#include "gtk/gtksignal.h"
#include "pgraph.h"
#include "spgraph.h"

static void spgraph_class_init          (SPGraphClass *klass);
static void spgraph_init                (SPGraph      *spgraph);
static void spgraph_size_allocate       (GtkWidget     *widget,
					 GtkAllocation *allocation);
static void spgraph_size_request         (GtkWidget      *widget,
					 GtkRequisition *requisition);

guint
spgraph_get_type ()
{
  static guint spgraph_type = 0;

  if (!spgraph_type)
    {
      GtkTypeInfo spgraph_info =
      {
	"SPGraph",
	sizeof (SPGraph),
	sizeof (SPGraphClass),
	(GtkClassInitFunc) spgraph_class_init,
	(GtkObjectInitFunc) spgraph_init,
        (GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL
      };

      spgraph_type = gtk_type_unique (gtk_vbox_get_type (), &spgraph_info);
    }

  return spgraph_type;
}

static void
spgraph_class_init (SPGraphClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  widget_class->size_allocate = spgraph_size_allocate;
  widget_class->size_request = spgraph_size_request;
  
}

static void
spgraph_adjustment_changed (GtkAdjustment *adjustment,
			    SPGraph       *spgraph)
{
  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (spgraph != NULL);
  g_return_if_fail (IS_SPGRAPH (spgraph));

  spgraph->offset=adjustment->value;
  pgraph_set_offset(PGRAPH(spgraph->pgraph),spgraph->offset);
}

static void
spgraph_init (SPGraph *spgraph)
{

  spgraph->offset = 0;

  spgraph->pgraph = pgraph_new ();
  gtk_box_pack_start (GTK_BOX(spgraph),spgraph->pgraph,0,0,0);
  gtk_widget_show (spgraph->pgraph);
		
  spgraph->adj = GTK_ADJUSTMENT(gtk_adjustment_new(0.0,0.0,100.0,
						   1.0,10.0,100.0));

  
  gtk_signal_connect (GTK_OBJECT(spgraph->adj),"changed",
		      (GtkSignalFunc)spgraph_adjustment_changed,
		      (gpointer)spgraph);
		      	      
  gtk_signal_connect (GTK_OBJECT(spgraph->adj),"value_changed",
		      (GtkSignalFunc)spgraph_adjustment_changed,
		      (gpointer)spgraph);

  spgraph->sb =  gtk_hscrollbar_new(spgraph->adj);

  gtk_box_pack_start (GTK_BOX(spgraph),spgraph->sb,0,0,0);
			    
}

static void 
spgraph_size_request (GtkWidget      *widget,
		      GtkRequisition *requisition)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_SPGRAPH (widget));

  gtk_widget_size_request(SPGRAPH(widget)->pgraph,requisition);
  requisition->width = 200;
}

GtkWidget*
spgraph_new ()
{
  return GTK_WIDGET ( gtk_type_new (spgraph_get_type ()));
}

static void
spgraph_size_allocate (GtkWidget     *widget,
		       GtkAllocation *allocation)
{
  SPGraph *spgraph;
  GtkAllocation child_allocation;
  GtkRequisition req;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (IS_SPGRAPH (widget));
  g_return_if_fail (allocation != NULL);
  
  widget->allocation = *allocation;
  if (GTK_WIDGET_REALIZED (widget))
    {
      spgraph = SPGRAPH (widget);

      gtk_widget_size_request(spgraph->pgraph,&req);

      if (req.width>allocation->width) {
	gtk_widget_show(spgraph->sb);
	child_allocation = *allocation;
	child_allocation.height-=spgraph->sb->requisition.height+5;
	gtk_widget_size_allocate (spgraph->pgraph,&child_allocation);
	child_allocation.y+=child_allocation.height+5;
	child_allocation.height=spgraph->sb->requisition.height;
	gtk_widget_size_allocate (spgraph->sb,&child_allocation);
	if (spgraph->offset>(req.width-allocation->width)) {
	  spgraph->adj->value=req.width-allocation->width;
	}
      } else {
	gtk_widget_hide(spgraph->sb);
	child_allocation = *allocation;
	gtk_widget_size_allocate (spgraph->pgraph,&child_allocation);
	spgraph->adj->value=0;
      }
      spgraph->adj->upper=req.width;
      spgraph->adj->page_size=allocation->width;
      spgraph->adj->page_increment=allocation->width;
      spgraph->adj->step_increment=req.width/100;
      gtk_signal_emit_by_name (GTK_OBJECT(spgraph->adj),"changed");
    }
}




